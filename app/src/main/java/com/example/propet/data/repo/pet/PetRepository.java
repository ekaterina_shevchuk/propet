package com.example.propet.data.repo.pet;

import android.content.Context;

import com.example.propet.data.repo.pet.source.mock.PetMockDataSource;
import com.example.propet.model.pet.Pet;

public class PetRepository implements IPetDataSource {
    private static PetRepository INSTANCE = null;
    private final IPetDataSource localDataSource;

    public static PetRepository getInstance(final Context context){
        if (null == INSTANCE) {
            INSTANCE = new PetRepository(context);
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    public PetRepository(final Context context){
        localDataSource = PetMockDataSource.getInstance(context);
    }

    @Override
    public void loadPetInfo(ILoadPetInfoCallback callback) {
        localDataSource.loadPetInfo(callback);
    }

    @Override
    public void postPet(IPostPetCallback callback, Pet pet) {
        localDataSource.postPet(callback, pet);
    }

    @Override
    public void loadPet(ILoadPetCallback callback, String id) {
        localDataSource.loadPet(callback, id);
    }

    public void deletePet(IDeletePetCallback callback, String id){
        localDataSource.deletePet(callback, id);
    }
}

