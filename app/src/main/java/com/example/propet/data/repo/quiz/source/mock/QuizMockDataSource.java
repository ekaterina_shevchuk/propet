package com.example.propet.data.repo.quiz.source.mock;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.propet.data.common.DataLayerError;
import com.example.propet.data.repo.quiz.IQuizDataSource;
import com.example.propet.model.pet.Pet;
import com.example.propet.model.quiz.InfoQuiz;
import com.example.propet.model.statistics.Quiz;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static android.provider.Contacts.SettingsColumns.KEY;

public class QuizMockDataSource implements IQuizDataSource {
    private static QuizMockDataSource INSTANCE;
    private static final int LOCAL_QUIZ_NUMBER = 5;
    private static final String LOG_TAG = QuizMockDataSource.class.getCanonicalName();

    private SharedPreferences preferences = null;
    private static final String NAME = "quiz_pref";
    private static final String KEY = "quiz";
    private static final Gson gson = new Gson();

    private static final InfoQuiz one =  new InfoQuiz("1","Ежедневный опрос", "Зафиксируйте состояние животного: оцените его здоровье, настроение, состояние шерсти и т.д.");
    private static final InfoQuiz two =  new InfoQuiz("2","Характер питомца", "Оцените характер вашего питомца. Это поможет другим пользователям при выборе друга.");

    public QuizMockDataSource(){

    }

    private QuizMockDataSource(final Context context) {
        if(this.preferences==null){
            this.preferences = context.getSharedPreferences(NAME,Context.MODE_PRIVATE);
            if(!preferences.contains(KEY)){
                List<InfoQuiz> quizSPList = new ArrayList<>();
                quizSPList.add(one);
                quizSPList.add(two);

                Type listType = new TypeToken<List<Pet>>() {}.getType();
                String target = gson.toJson(quizSPList, listType);
                preferences.edit().putString(KEY,target).apply();
            }
        }
    }

    public static QuizMockDataSource getInstance(final Context context) {
        if (null == INSTANCE) {
            INSTANCE = new QuizMockDataSource(context);
        }
        return INSTANCE;
    }

    public static void destroyInstance(){
        INSTANCE = null;
    }


    @Override
    public void loadQuizInfo(ILoadQuizInfoCallback callback) {
        List<InfoQuiz> infoQuiz = new ArrayList<>();
        infoQuiz.add(one);
        infoQuiz.add(two);

        //infoQuiz=null;//для didFailLoad

        if(infoQuiz!=null){
            callback.didLoad(infoQuiz);
        }else {
            callback.didFailLoad(new DataLayerError("500", "Internal Server Error"));
        }
    }

    @Override
    public void loadQuiz(ILoadQuizCallback callback, String id) {
        InfoQuiz q = getInfoQuiz(id);
        if(q!=null){
            callback.didLoad(q);
        }
        else {
            callback.didFailLoad(new DataLayerError("404", "Not Found"));
        }
    }

    private static InfoQuiz getInfoQuiz(String id){
        switch (id){
            case "1": return one;
            case "2": return two;
            default:return null;
        }
    }


}

