package com.example.propet.data.repo.statistic;

import android.content.Context;

import com.example.propet.data.repo.statistic.source.mock.StatisticMockDataSource;
import com.example.propet.model.statistics.Quiz;

public class StatisticRepository implements IStatisticDataSource {
    private static StatisticRepository INSTANCE = null;
    private final IStatisticDataSource localDataSource;


    public static StatisticRepository getInstance(final Context context){
        if (null == INSTANCE) {
            INSTANCE = new StatisticRepository(context);
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    public StatisticRepository(final Context context){
        localDataSource = StatisticMockDataSource.getInstance(context);
    }

    @Override
    public void loadPetStatistic(ILoadPetStatisticCallback callback, String petId) {
        localDataSource.loadPetStatistic(callback, petId);
    }

    @Override
    public void postPetStatistic(IPostPetStatisticCallback callback, Quiz quiz) {
        localDataSource.postPetStatistic(callback,quiz);
    }

    @Override
    public void loadPetQuizStatistic(ILoadPetQuizStatisticCallback callback, String petId, String quizId) {
        loadPetQuizStatistic(callback, petId, quizId);
    }
}

