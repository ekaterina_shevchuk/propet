package com.example.propet.data.common;

public class DataLayerError {

    private String code;

    private String msg;
    public DataLayerError(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public String getCode() {
        return code;
    }


}
