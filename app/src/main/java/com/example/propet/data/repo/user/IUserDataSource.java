package com.example.propet.data.repo.user;

import android.content.Context;

import com.example.propet.data.common.DataLayerError;
import com.example.propet.model.news.News;
import com.example.propet.model.user.User;

import java.util.List;

/**
 * Единый интерфейс для всех источников данных типа <User>.
 *
 */
public interface IUserDataSource {

    /**
     * Метод для регистрации пользоватля
     * @param callback колбэк
     */
    void registerUser(final IRegisterCallback callback, User user);

    /**
     * Ошибка — Error типа DataLayerError.
     */
    interface IRegisterCallback {
        void didRegister();
        void didFailRegister(final DataLayerError error);
    }

    /**
     * Метод для регистрации нового пользователя
     * @param callback колбэк
     */
    void authorizeUser(final IAuthorizeCallback callback,User user);

    /**
     * Ошибка — Error типа DataLayerError.
     */
    interface IAuthorizeCallback {
        void didAuthorize();
        void didFailAuthorize(final DataLayerError error);
    }

    /**
     * Восстановление пароля
     * @param callback колбэк
     * @param email почта
     */
    void recoverPassword(final IRecoverCallback callback,String email);

    interface IRecoverCallback {
        void didRecover();
        void didFailRecover(DataLayerError error);
    }

}
