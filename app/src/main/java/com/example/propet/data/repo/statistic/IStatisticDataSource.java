package com.example.propet.data.repo.statistic;

import com.example.propet.data.common.DataLayerError;
import com.example.propet.model.statistics.Quiz;
import com.example.propet.model.statistics.Statistics;

import java.util.List;

public interface IStatisticDataSource {

    /**
     * Загрузить статистику питомца по все пройденным для него квизам
     * @param callback колбэк
     * @param petId id питомца
     */
    void loadPetStatistic(final ILoadPetStatisticCallback callback, String petId);

    interface ILoadPetStatisticCallback {
        void didLoad(final List<Statistics> statisticsList);
        void didFailLoad(final DataLayerError error);
    }

    /**Сохранить новую статистику по питомцу по конкретному квизу
     *
     * @param callback колбэк
     * @param quiz квиз (содержит результат прохождения квиза)
     */
    void postPetStatistic(final IPostPetStatisticCallback callback, Quiz quiz);

    interface IPostPetStatisticCallback {
        void didLoad();
        void didFailLoad(final DataLayerError error);
    }

    /**
     * Загрузить статистику питомца по конкретному квизу
     * @param callback колбэк
     * @param petId id питомца
     * @param quizId id квиза
     */
    void loadPetQuizStatistic(final ILoadPetQuizStatisticCallback callback,String petId, String quizId);

    interface ILoadPetQuizStatisticCallback {
        void didLoad(final Statistics statistics);
        void didFailLoad(final DataLayerError error);
    }
}
