package com.example.propet.data.repo.quiz;

import com.example.propet.data.common.DataLayerError;
import com.example.propet.model.quiz.InfoQuiz;
import com.example.propet.model.statistics.Quiz;

import java.util.List;

public interface IQuizDataSource {
    /**
     * Получить инфу о квизе для показа в списке
     *
     * @param callback колбэк
     */
    void loadQuizInfo(final ILoadQuizInfoCallback callback);

    /**
     * Загрузить инфу о конкретном квизе
     *
     * @param callback колбэк
     * @param id       id квиза
     */
    void loadQuiz(final ILoadQuizCallback callback, String id);

    interface ILoadQuizInfoCallback {
        void didLoad(final List<InfoQuiz> quiz);

        void didFailLoad(final DataLayerError error);
    }

    interface ILoadQuizCallback {
        void didLoad(final InfoQuiz quiz);

        void didFailLoad(final DataLayerError error);
    }
}

