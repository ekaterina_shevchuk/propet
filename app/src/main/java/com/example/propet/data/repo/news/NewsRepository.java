package com.example.propet.data.repo.news;

import com.example.propet.data.repo.news.source.mock.NewsMockDataSource;

public final class NewsRepository implements INewsDataSource {
    private static NewsRepository INSTANCE = null;
    private final INewsDataSource localDataSource;


    public static NewsRepository getInstance(){
        if (null == INSTANCE) {
            INSTANCE = new NewsRepository();
        }
        return INSTANCE;
    }

    /**
     * выбирает,какой источник использовать, у нас только мок
     */
    public NewsRepository(){
        localDataSource = NewsMockDataSource.getInstance();
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    @Override
    public void loadNews(ILoadNewsCallback callback) {
        localDataSource.loadNews(callback);
    }

    @Override
    public void loadOneNews(ILoadOneNewsCallback callback, String newsId) {
        localDataSource.loadOneNews(callback, newsId);
    }
}

