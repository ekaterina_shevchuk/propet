package com.example.propet.data.repo.news.source.mock;

import com.example.propet.data.common.DataLayerError;
import com.example.propet.data.repo.news.INewsDataSource;
import com.example.propet.model.news.News;
import com.example.propet.model.news.NewsPriority;

import java.util.ArrayList;
import java.util.List;

public final class NewsMockDataSource implements INewsDataSource {
    private static NewsMockDataSource INSTANCE;
    private static final String LOG_TAG = NewsMockDataSource.class.getCanonicalName();

    private static final News one =  new News(
            "1",
            NewsPriority.NOTE_PRIORITY_HIGH,
            "Дружба кошек и собак",
            "10.10.2020",
            "Для начала стоит упомянуть, что они принадлежат к разным видам. Для собак кошки является существами \"из другого измерения\". Грубо говоря, между ними происходит простое недопонимание. При поглаживании кошка выгибает спину и мурлыкает - ей приятно, она рада, а когда собака пытается выгнуть спину и начинает слегка рычать - это плохой знак. Если собака виляет ховстом - она показывает свою радость. Если у кошки хвост начинает ходить ходуном - она зла. Эти причины являются главными, по которым собаки не могут понять, почему кошка агрессивна, а представитель семейства кошачьих в недоумении - где радость? Так вот, если кошка сделает какое-то движение, которое для нее является обыденным, то собака может это воспринимать неправильно и кинуться в атаку.",
            new byte[]{});
    private static final News two =  new News(
            "2",
            NewsPriority.NOTE_PRIORITY_LOW,
            "Хомяки обыкновенные",
            "11.11.2017",
            "Самый крупный вид домашних хомяков. Эти грызуны агрессивны, свободолюбивы и  рьяно охраняют  территорию. Будьте готовы к тому, что хомяк выберет одного хозяина среди членов семьи и признает только его, агрессивно защищая себя и домик от других людей. Из плюсов - оригинальная трехцветная окраска, активное поведение, за которым интересно наблюдать.",
            new byte[]{});
    private static final News three =  new News(
            "3",
            NewsPriority.NOTE_PRIORITY_HIGH,
            "3 причины почему на вас лают собаки",
            "08.15.2018",
            "Территориальный или защитный. Собака гавкает, когда человек или животное попадает на охраняемую территорию. Когда «угроза» приближается к собаке, лай становится громче, напористее, отрывистее. Внешне, собака будет выглядеть сконцентрированной, угрожающей или даже агрессивной.\n" +
                    "\n" +
                    "· Тревога или страх. Некоторые собаки лают, раздражаясь от любого шума или увидев незнакомый предмет. Так называемый панический лай – это эмоция, которая не зависит от того находится ли собака на своей территории.\n" +
                    "\n" +
                    "· Скука или одиночество. Собаки – это стайные, очень социальные животные. Если четвероногий часто и надолго остается один, он будет гавкать и даже выть от тоски..",
            new byte[]{});

    private NewsMockDataSource() {}

    public static NewsMockDataSource getInstance() {
        if (null == INSTANCE) {
            INSTANCE = new NewsMockDataSource();
        }
        return INSTANCE;
    }

    public static void destroyInstance(){
        INSTANCE = null;
    }

    @Override
    public void loadNews(ILoadNewsCallback callback) {
        List<News> infoQuiz = new ArrayList<>();
        infoQuiz.add(one);
        infoQuiz.add(two);
        infoQuiz.add(three);

        if(infoQuiz!=null){
            callback.didLoad(infoQuiz);
        }
        else {
            callback.didFailLoad(new DataLayerError("500", "Internal Server Error"));
        }
    }

    @Override
    public void loadOneNews(ILoadOneNewsCallback callback, String newsId) {
        News news = getNews(newsId);

        if(news!=null){
            callback.didLoad(news);
        }else {
            callback.didFailLoad(new DataLayerError("404", "Not Found"));
        }
    }

    private static News getNews(String id){
        switch (id){
            case "1": return one;
            case "2": return two;
            case "3":return three;
            default: return null;
        }
    }


}

