package com.example.propet.data.repo.user;

import android.content.Context;

import com.example.propet.data.repo.user.source.mock.UserMockDataSource;
import com.example.propet.model.user.User;

public class UserRepository implements IUserDataSource{
    private static UserRepository INSTANCE;
    private final IUserDataSource mockDataSource;


    public static UserRepository getInstance(final Context context){
        if (null == INSTANCE) {
            INSTANCE = new UserRepository(context);
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    public UserRepository(final Context context){
        mockDataSource = UserMockDataSource.getInstance(context);
    }


    @Override
    public void registerUser(IRegisterCallback callback, User user) {
        mockDataSource.registerUser(callback,user);
    }

    @Override
    public void authorizeUser(IAuthorizeCallback callback, User user) {
        mockDataSource.authorizeUser(callback,user);
    }

    @Override
    public void recoverPassword(IRecoverCallback callback, String email) {
        mockDataSource.recoverPassword(callback,email);
    }
}
