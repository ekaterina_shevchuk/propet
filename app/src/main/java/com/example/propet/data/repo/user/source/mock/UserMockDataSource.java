package com.example.propet.data.repo.user.source.mock;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.propet.data.common.DataLayerError;
import com.example.propet.data.repo.user.IUserDataSource;
import com.example.propet.model.user.User;
import com.google.gson.Gson;

public class UserMockDataSource implements IUserDataSource {
    private static UserMockDataSource INSTANCE;
    private static final String LOG_TAG = UserMockDataSource.class.getCanonicalName();

    private final SharedPreferences preferences;
    private static final String NAME = "user_pref";
    private static final String KEY = "user";
    private static final Gson gson  = new Gson();

    private static final User user = new User("petr@g.ru","123");

    public static UserMockDataSource getInstance(final Context context) {
        if (null == INSTANCE) {
            INSTANCE = new UserMockDataSource(context);
        }
        return INSTANCE;
    }

    public UserMockDataSource(final Context context) {
        this.preferences =  context.getSharedPreferences(NAME,Context.MODE_PRIVATE);
        if(!preferences.contains(KEY)){
            String target = gson.toJson(user, User.class);
            this.preferences.edit().putString(KEY,target).apply();
        }
    }

    @Override
    public void registerUser(IRegisterCallback callback,User user) {
        if(registerUser(user)){
            callback.didRegister();
        }else{
            callback.didFailRegister(new DataLayerError("401", "Unauthorized"));
        }
    }

    @Override
    public void authorizeUser(IAuthorizeCallback callback,User user) {
        if(authUser(user)){
            callback.didAuthorize();
        }else{
            callback.didFailAuthorize(new DataLayerError("401", "Unauthorized"));
        }
    }

    @Override
    public void recoverPassword(IRecoverCallback callback, String email) {
        if(!preferences.contains(KEY)) {
            callback.didFailRecover(new DataLayerError("404","email not found"));
            return;
        }
        User u = gson.fromJson(preferences.getString(KEY, ""), User.class);
        if(!u.getEmail().equals(email)){
            callback.didFailRecover(new DataLayerError("404","email not found"));
            return;
        }
        callback.didRecover();
    }

    private boolean registerUser(User user){
        String email = user.getEmail();
        User  u = null;
        if(preferences.contains(KEY)) {
            u = gson.fromJson(preferences.getString(KEY, ""), User.class);
        }

        if( u!=null && u.getEmail().equals(email)){
            return false;
        }else {
            String userS = gson.toJson(user);
            preferences.edit().putString(KEY,userS).apply();
            return true;
        }
    }

    private boolean authUser(User user){
        if(!preferences.contains(KEY)){
            return false;
        }
        User u = gson.fromJson(preferences.getString(KEY, ""), User.class);

        boolean b1 = u.getEmail().equals(user.getEmail());
        boolean b2 = u.getPassword().equals(user.getPassword());

        return b1 && b2;
    }

}
