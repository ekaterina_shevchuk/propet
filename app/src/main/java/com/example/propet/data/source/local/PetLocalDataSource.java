package com.example.propet.data.source.local;

import com.example.propet.data.repo.pet.IPetDataSource;
import com.example.propet.data.repo.pet.PetRepository;

public class PetLocalDataSource/* implements IPetDataSource*/ {
    private static PetRepository INSTANCE;
    private static final int LOCAL_PET_NUMBER = 10;
    private static final String LOG_TAG = PetLocalDataSource.class.getCanonicalName();

    private PetLocalDataSource() {

    }

//    public static PetRepository getInstance() {
//        if (null == INSTANCE) {
//            INSTANCE = new PetRepository();
//        }
//        return INSTANCE;
//    }
//
//    public static void destroyInstance(){
//        INSTANCE = null;
//    }
//
//    @Override
//    public void loadPetInfo(ILoadPetCallback callback) {
//
//    }
}

