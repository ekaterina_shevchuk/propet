package com.example.propet.data.repo.news;

import com.example.propet.data.common.DataLayerError;
import com.example.propet.model.news.News;

import java.util.List;

/**
 * Единый интерфейс для всех источников данных типа <News>.
 *
 * Обычно используется две модели - усеченные новости и полная новость,
 * где усеченные используются для показа списка,
 * а полная новость для показа конкретной новости
 *
 * Но так как не было решено, как эти две модели различаются, то используется одна модель
 */
public interface INewsDataSource {


    /**
     * Загрузить все доступные новости
     * @param callback колбэк
     */
    void loadNews(final ILoadNewsCallback callback);


    interface ILoadNewsCallback {
        void didLoad(final List<News> notes);
        void didFailLoad(final DataLayerError error);
    }

    /**
     * Загрузить конкретную новость полностью
     * @param callback колбэк
     * @param newsId id новости
     */
    void loadOneNews(final ILoadOneNewsCallback callback, final String newsId);

    interface ILoadOneNewsCallback {
        void didLoad(final News notes);
        void didFailLoad(final DataLayerError error);
    }
}
