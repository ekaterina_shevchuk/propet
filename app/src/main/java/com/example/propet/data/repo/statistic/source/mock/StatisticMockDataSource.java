package com.example.propet.data.repo.statistic.source.mock;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.propet.data.common.DataLayerError;
import com.example.propet.data.repo.statistic.IStatisticDataSource;
import com.example.propet.model.common.Value;
import com.example.propet.model.quiz.InfoQuiz;
import com.example.propet.model.statistics.Quiz;
import com.example.propet.model.statistics.Statistics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StatisticMockDataSource implements IStatisticDataSource {
    private static StatisticMockDataSource INSTANCE;
    private static final int LOCAL_STATISTIC_NUMBER = 15;
    private static final String LOG_TAG = StatisticMockDataSource.class.getCanonicalName();

    private final SharedPreferences preferences;
    private static final String NAME = "stat_pref";
    private static final String KEY = "stat";

    private static final Gson gson = new Gson();

    private static final Quiz one =  new Quiz("1","1", Value.SOFT,"25.12.2020");
    private static final Quiz two =  new Quiz("1","1",Value.SMILE,"25.12.2020");
    private static final Quiz three =  new Quiz("1","1",Value.SMOOTH,"26.12.2020");
    private static final Quiz four =  new Quiz("1","1",Value.SLEEPY,"26.12.2020");
    private static final Quiz five =  new Quiz("1","1",Value.HARD,"27.12.2020");
    private static final Quiz six =  new Quiz("1","1",Value.ANGRY,"27.12.2020");

    private static final Quiz one1 =  new Quiz("1","2",Value.LOW,"25.12.2020");
    private static final Quiz two1 =  new Quiz("1","2",Value.LOW,"25.12.2020");
    private static final Quiz three1 =  new Quiz("1","2",Value.MEDIUM,"26.12.2020");
    private static final Quiz four1 =  new Quiz("1","2",Value.LOW,"26.12.2020");
    private static final Quiz five1 =  new Quiz("1","2",Value.LOW,"27.12.2020");
    private static final Quiz six1 =  new Quiz("1","2",Value.LOW,"27.12.2020");


    private static final InfoQuiz oneQ =  new InfoQuiz("1","Ежедневный опрос", "");
    private static final InfoQuiz twoQ =  new InfoQuiz("2","Характер питомца", "");


    private StatisticMockDataSource(final Context context) {
        this.preferences = context.getSharedPreferences(NAME,Context.MODE_PRIVATE);
        if(!preferences.contains(KEY)){
            List<Quiz> petSPList = new ArrayList<>();
            petSPList.add(one);
            petSPList.add(two);
            petSPList.add(three);
            petSPList.add(four);
            petSPList.add(five);
            petSPList.add(six);

            petSPList.add(one1);
            petSPList.add(two1);
            petSPList.add(three1);
            petSPList.add(four1);
            petSPList.add(five1);
            petSPList.add(six1);

            Type listType = new TypeToken<List<Statistics>>() {}.getType();
            String target = gson.toJson(petSPList, listType);
            this.preferences.edit().putString(KEY,target).apply();
        }
    }

    public static StatisticMockDataSource getInstance(final Context context) {
        if (null == INSTANCE) {
            INSTANCE = new StatisticMockDataSource(context);
        }
        return INSTANCE;
    }

    public static void destroyInstance(){
        INSTANCE = null;
    }


    @Override
    public void loadPetStatistic(ILoadPetStatisticCallback callback, String petId) {
        Statistics one = getStat(petId,"1");
        Statistics two = getStat(petId,"2");
        Statistics three = getStat(petId,"3");

        List<Statistics> list = new ArrayList<>();
        //вообще проверка не нужна, так как мы знаем, что тут все будет не ноль
        if(one!=null){list.add(one);}
        if(two!=null){list.add(two);}
        if(three!=null){list.add(three);}

        //list=null;//для didFailLoad

        if(list!=null){
            callback.didLoad(list);
        }else {
            callback.didFailLoad(new DataLayerError("500", "Internal Server Error"));
        }
    }

    @Override
    public void postPetStatistic(IPostPetStatisticCallback callback, Quiz quiz) {
        Type listType = new TypeToken<List<Quiz>>() {}.getType();
        List<Quiz> target = gson.fromJson(preferences.getString(KEY,""), listType);
        target.add(quiz);
        String json = gson.toJson(target, listType);
        preferences.edit().putString(KEY,json).apply();
        callback.didLoad();
    }

    @Override
    public void loadPetQuizStatistic(ILoadPetQuizStatisticCallback callback, String petId, String quizId) {
        Statistics statistics =  getStat(petId, quizId);
        if(statistics!=null){
            callback.didLoad(statistics);
        }else {
            callback.didFailLoad(new DataLayerError("404", "Not Found"));
        }
    }

    private Statistics getStat(String petId, String quizId){
        Type listType = new TypeToken<List<Quiz>>() {}.getType();
        List<Quiz> target = gson.fromJson(preferences.getString(KEY,""), listType);

        Map<String, Value> petStat = new HashMap<>();
        for (Quiz q : target) {
            if(q.getPetId().equals(petId) && q.getQuizId().equals(quizId)){
                petStat.put(q.getDate(),q.getValue());
            }
        }

        InfoQuiz infoQuiz = getQuiz(quizId);
        if(infoQuiz!=null){
            return new Statistics(infoQuiz.getTitle(),petStat);
        }
        else {
            return null;
        }
    }

    private static InfoQuiz getQuiz(String id){
        switch (id){
            case "1": return oneQ;
            case "2": return twoQ;
            default:return null;
        }
    }

}

