package com.example.propet.data.repo.pet;

import android.content.Context;

import com.example.propet.data.common.DataLayerError;
import com.example.propet.model.pet.InfoPet;
import com.example.propet.model.pet.Pet;

import java.util.List;

public interface IPetDataSource {
    /**
     * Получить краткую инфу по всем питомцам
     * @param callback колбэк
     */
    void loadPetInfo(final ILoadPetInfoCallback callback);

    interface ILoadPetInfoCallback {
        void didLoad(final List<InfoPet> pet);
        void didFailLoad(final DataLayerError error);
    }

    /**
     * Сохранить нового питомца
     * @param callback колбэк
     * @param pet питомец
     */
    void postPet(IPostPetCallback callback, final Pet pet);

    interface IPostPetCallback {
        void didLoad();
        void didFailLoad(final DataLayerError error);
    }

    /**
     * Получить все информацию конкретного питомца по его id
     * @param callback колбэк
     * @param id питомца
     */
    void loadPet(ILoadPetCallback callback, String id);

    interface ILoadPetCallback {
        void didLoad(final Pet pet);
        void didFailLoad(final DataLayerError error);
    }

    void deletePet(IDeletePetCallback callback, String id);

    interface IDeletePetCallback {
        void didDelete(final String id);
        void didFailDelete(final DataLayerError error);
    }
}
