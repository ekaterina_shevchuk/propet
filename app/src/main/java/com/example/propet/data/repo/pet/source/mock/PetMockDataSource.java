package com.example.propet.data.repo.pet.source.mock;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.propet.data.common.DataLayerError;
import com.example.propet.data.repo.pet.IPetDataSource;
import com.example.propet.model.pet.InfoPet;
import com.example.propet.model.pet.Pet;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class PetMockDataSource implements IPetDataSource {
    private static PetMockDataSource INSTANCE;
    private static final int LOCAL_PET_NUMBER = 10;
    private static final String LOG_TAG = PetMockDataSource.class.getCanonicalName();

    private SharedPreferences preferences = null;
    private static final String NAME = "pet_pref";
    private static final String KEY = "pet";
    private static final Gson gson = new Gson();

    private static final Pet one =  new Pet("1","Снежок",5, "м", 5,"не любит лосось","овчарка",new byte[]{});
    private static final Pet two =  new Pet("2","Пушок",3,"м", 3,"не породистый","не известно",new byte[]{});
    private static final Pet three =  new Pet( "3","Уголек",2,"м", 7,"сходит с ума от кошачей мяты","британец",new byte[]{});

    private PetMockDataSource(final Context context) {
        if(this.preferences==null){
            this.preferences = context.getSharedPreferences(NAME,Context.MODE_PRIVATE);
            if(!preferences.contains(KEY)){
                List<Pet> petSPList = new ArrayList<>();
                petSPList.add(one);
                petSPList.add(two);
                petSPList.add(three);

                Type listType = new TypeToken<List<Pet>>() {}.getType();
                String target = gson.toJson(petSPList, listType);
                preferences.edit().putString(KEY,target).apply();
            }
        }
    }

    public static PetMockDataSource getInstance(final Context context) {
        if (null == INSTANCE) {
            INSTANCE = new PetMockDataSource(context);
        }
        return INSTANCE;
    }

    public static void destroyInstance(){
        INSTANCE = null;
    }

    @Override
    public void loadPetInfo(ILoadPetInfoCallback callback) {
        Type listType = new TypeToken<List<Pet>>() {}.getType();
        String pref_str = preferences.getString(KEY,"");
        List<Pet> list = gson.fromJson(pref_str,listType);

        List<InfoPet> infoPets = new ArrayList<>();
       // List<InfoPet> infoPets = null;//для didFailLoad

        for (Pet pet : list) {
            InfoPet infoPet = new InfoPet(pet.getId(), pet.getName(),pet.getImageBytes());
            infoPets.add(infoPet);
        }

        if(infoPets!=null){
            callback.didLoad(infoPets);
        }else {
            callback.didFailLoad(new DataLayerError("500", "Internal Server Error"));
        }
    }

    @Override
    public void postPet(IPostPetCallback callback, Pet pet) {
        savePet(pet);
        callback.didLoad();
        //callback.didFailLoad(new DataLayerError("000", "smth"));
    }

    @Override
    public void loadPet(ILoadPetCallback callback, String id) {
        Pet pet = getPet(id);
        if(pet!=null){
            callback.didLoad(pet);
        }else {
            callback.didFailLoad(new DataLayerError("404", "Not Found"));
        }
    }

    @Override
    public void deletePet(IDeletePetCallback callback, String id) {
        deletePetInfo(id);
        callback.didDelete(id);
    }

    private Pet getPet(String id){
        Type listType = new TypeToken<List<Pet>>() {}.getType();
        List<Pet> list = gson.fromJson(preferences.getString(KEY,""),listType);

        for (Pet pet : list) {
            if(pet.getId().equals(id)){
                return pet;
            }
        }
        return null;
    }

    private void savePet(Pet pet){
        Type listType = new TypeToken<List<Pet>>() {}.getType();
        List<Pet> list = gson.fromJson(preferences.getString(KEY,""),listType);
        Integer maxId = 0;
        for (Pet p : list) {
            if(Integer.parseInt(p.getId()) > maxId){
                maxId = Integer.parseInt(p.getId());
            }
        }
        maxId++;
        pet.setId(maxId.toString());
        list.add(pet);

        String target = gson.toJson(list, listType);
        preferences.edit().putString(KEY,target).apply();
    }

    private void  deletePetInfo(String id){
        Type listType = new TypeToken<List<Pet>>() {}.getType();
        List<Pet> list = gson.fromJson(preferences.getString(KEY,""),listType);

        Pet pet = null;
        for (Pet p : list) {
            if(p.getId().equals(id)){
                pet = p;
            }
        }

        list.remove(pet);

        String target = gson.toJson(list, listType);
        preferences.edit().putString(KEY,target).apply();
    }

}

