package com.example.propet.data.repo.quiz;

import android.content.Context;

import com.example.propet.data.repo.quiz.source.mock.QuizMockDataSource;

public class QuizRepository implements IQuizDataSource {
    private static QuizRepository INSTANCE = null;
    private final IQuizDataSource localDataSource;


    public static QuizRepository getInstance(final Context context){
        if (null == INSTANCE) {
            INSTANCE = new QuizRepository(context);
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }


    public QuizRepository(final Context context){
        localDataSource = QuizMockDataSource.getInstance(context);
    }

    @Override
    public void loadQuizInfo(ILoadQuizInfoCallback callback) {
        localDataSource.loadQuizInfo(callback);
    }

    @Override
    public void loadQuiz(ILoadQuizCallback callback, String id) {
        localDataSource.loadQuiz(callback, id);
    }
}

