package com.example.propet.domain;

import com.example.propet.data.common.DataLayerError;
import com.example.propet.data.repo.user.IUserDataSource;
import com.example.propet.domain.common.DomainLayerError;
import com.example.propet.domain.common.UseCase;

public class RecoveryPasswordUseCase extends UseCase<RecoveryPasswordUseCase.RequestValues,RecoveryPasswordUseCase.ResponseValues>{
    private final IUserDataSource userDataSource;

    public RecoveryPasswordUseCase(IUserDataSource userDataSource) {
        this.userDataSource = userDataSource;
    }

    @Override
    protected void execute(RequestValues parameters) {
        userDataSource.recoverPassword(new IUserDataSource.IRecoverCallback() {
            @Override
            public void didRecover() {
                final RecoveryPasswordUseCase.ResponseValues responseValues = new RecoveryPasswordUseCase.ResponseValues();
                getCallback().onSuccess(responseValues);
            }

            @Override
            public void didFailRecover(DataLayerError error) {
                final DomainLayerError domainLayerError = getDomainLayerError(error);
                getCallback().onError(domainLayerError);
            }
        },parameters.getEmail());
    }


    public static final class RequestValues implements UseCase.IRequestValues {

        private final String email;


        public RequestValues(String email) {
            this.email = email;
        }

        public String getEmail() {
            return email;
        }
    }

    public static final class ResponseValues implements UseCase.IResponseValues {
        ResponseValues(){}
    }

    private DomainLayerError getDomainLayerError(final DataLayerError dataLayerError) {
        String msg;
        switch (dataLayerError.getCode()){
            case "401":msg="Ошибка авторизации";break;
            case "404":msg="Объект не был найден";break;
            case "500":msg="Серевер болен :(";break;
            default:msg="Упс, что-то произошло не так";break;
        }
        return new DomainLayerError(msg);
    }
}
