package com.example.propet.domain;

import com.example.propet.data.common.DataLayerError;
import com.example.propet.data.repo.pet.IPetDataSource;
import com.example.propet.domain.common.DomainLayerError;
import com.example.propet.domain.common.UseCase;
import com.example.propet.model.pet.InfoPet;
import com.example.propet.model.pet.Pet;

public class LoadPetInfoUseCase extends UseCase<LoadPetInfoUseCase.RequestValues, LoadPetInfoUseCase.ResponseValues>{
    private final IPetDataSource petDataSource;

    public LoadPetInfoUseCase(IPetDataSource petDataSource) {
        this.petDataSource = petDataSource;
    }

    @Override
    protected void execute(LoadPetInfoUseCase.RequestValues parameters) {

        petDataSource.loadPet(new IPetDataSource.ILoadPetCallback(){
            @Override
            public void didLoad(Pet pet) {
                final LoadPetInfoUseCase.ResponseValues responseValues = new LoadPetInfoUseCase.ResponseValues(pet);
                getCallback().onSuccess(responseValues);
            }

            @Override
            public void didFailLoad(DataLayerError error) {
                final DomainLayerError domainLayerError = getDomainLayerError(error);
                getCallback().onError(domainLayerError);
            }
        }, parameters.getId());
    }

    public static final class RequestValues implements UseCase.IRequestValues {

        private final String id;

        public RequestValues(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }
    }

    public static final class ResponseValues implements UseCase.IResponseValues {

        private final Pet pet;

        public ResponseValues(Pet pet) {
            this.pet = pet;
        }

        public Pet getPet() {
            return pet;
        }
    }

    private DomainLayerError getDomainLayerError(final DataLayerError dataLayerError) {
        String msg;
        switch (dataLayerError.getCode()){
            case "404":msg="Объект не был найден";break;
            case "500":msg="Серевер болен :(";break;
            default:msg="Упс, что-то произошло не так";break;
        }
        return new DomainLayerError(msg);
    }
}
