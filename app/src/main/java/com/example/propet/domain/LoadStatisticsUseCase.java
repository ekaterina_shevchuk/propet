package com.example.propet.domain;

import com.example.propet.data.common.DataLayerError;
import com.example.propet.data.repo.statistic.IStatisticDataSource;
import com.example.propet.domain.common.DomainLayerError;
import com.example.propet.domain.common.UseCase;
import com.example.propet.model.statistics.Statistics;

import java.util.List;

public class LoadStatisticsUseCase extends UseCase<LoadStatisticsUseCase.RequestValues, LoadStatisticsUseCase.ResponseValues> {
    private final IStatisticDataSource statisticsRepository;

    public LoadStatisticsUseCase(final IStatisticDataSource statisticsRepository) {
        this.statisticsRepository = statisticsRepository;
    }

    @Override
    protected void execute(RequestValues parameters) {
        //TODO: так как мы можем получать статистику по одному питомцу или по конкретному питомцу+конкретному квизу, то нужно переписать то, что было закоменчено тут
    }

    /*@Override
    protected void execute(final RequestValues parameters) {
        statisticsRepository.loadStatisticInfo(new IStatisticDataSource.ILoadStatisticCallback() {

            // На уровне Repository нам удалось загрузить/получить notes.
            @Override
            public void didLoad(final List<IStatistics> statistics) {
                final ResponseValues responseValues = new ResponseValues(statistics);
                // Этот callback мы передавали из Presenter в UseCaseExecutor.
                getCallback().onSuccess(responseValues);
            }

            @Override
            public void didFailLoad(final DataLayerError error) {
                final DomainLayerError domainLayerError = getDomainLayerError(error);
                getCallback().onError(domainLayerError);
            }
        });
    }*/

    //region Request and Response types.
    public static final class RequestValues implements UseCase.IRequestValues {
        // Входные данные на данном этапе не нужны.
        // Но для реализации UseCase'а нужна хотя бы пустая реализация.
    }

    public static final class ResponseValues implements UseCase.IResponseValues {
        private final List<Statistics> statistics;

        public ResponseValues(final List<Statistics> statistics) {
            this.statistics = statistics;
        }

        public List<Statistics> getStatistics() {
            return statistics;
        }

    }
    //endregion

    private DomainLayerError getDomainLayerError(final DataLayerError dataLayerError) {
        // TODO: Method without implementation.
        return null;
    }
}
