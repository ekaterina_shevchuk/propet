package com.example.propet.domain;

import com.example.propet.data.common.DataLayerError;
import com.example.propet.data.repo.quiz.IQuizDataSource;
import com.example.propet.domain.common.DomainLayerError;
import com.example.propet.domain.common.UseCase;
import com.example.propet.model.quiz.InfoQuiz;

public class Quiz11UseCase extends UseCase<Quiz11UseCase.RequestValues, Quiz11UseCase.ResponseValues>{
    private final IQuizDataSource quizDataSource;

    public Quiz11UseCase(IQuizDataSource quizDataSource) {
        this.quizDataSource = quizDataSource;
    }

    @Override
    protected void execute(Quiz11UseCase.RequestValues parameters) {

        quizDataSource.loadQuiz(new IQuizDataSource.ILoadQuizCallback() {
            @Override
            public void didLoad(InfoQuiz infoQuiz) {
                final Quiz11UseCase.ResponseValues responseValues = new Quiz11UseCase.ResponseValues(infoQuiz);
                getCallback().onSuccess(responseValues);
            }

            @Override
            public void didFailLoad(DataLayerError error) {
                final DomainLayerError domainLayerError = getDomainLayerError(error);
                getCallback().onError(domainLayerError);
            }
        }, parameters.getId());
    }

    public static final class RequestValues implements UseCase.IRequestValues {

        private final String id;

        public RequestValues(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }

    }

    public static final class ResponseValues implements UseCase.IResponseValues {
        private final InfoQuiz infoQuiz;

        public ResponseValues(InfoQuiz infoQuiz) {
            this.infoQuiz = infoQuiz;
        }

        public InfoQuiz getInfoQuiz() {
            return infoQuiz;
        }
    }

    private DomainLayerError getDomainLayerError(final DataLayerError dataLayerError) {
        String msg;
        switch (dataLayerError.getCode()){
            case "000":msg="Ответы не сохранены";break;
            case "404":msg="Объект не был найден";break;
            case "500":msg="Серевер болен :(";break;
            default:msg="Упс, что-то произошло не так";break;
        }
        return new DomainLayerError(msg);
    }
}
