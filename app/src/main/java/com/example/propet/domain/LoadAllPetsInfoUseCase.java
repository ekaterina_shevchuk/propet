package com.example.propet.domain;

import com.example.propet.data.common.DataLayerError;
import com.example.propet.data.repo.pet.IPetDataSource;
import com.example.propet.domain.common.DomainLayerError;
import com.example.propet.domain.common.UseCase;
import com.example.propet.model.pet.InfoPet;

import java.util.List;

public class LoadAllPetsInfoUseCase extends UseCase<LoadAllPetsInfoUseCase.RequestValues, LoadAllPetsInfoUseCase.ResponseValues>{
    private final IPetDataSource petDataSource;

    public LoadAllPetsInfoUseCase(IPetDataSource petDataSource) {
        this.petDataSource = petDataSource;
    }

    @Override
    protected void execute(LoadAllPetsInfoUseCase.RequestValues parameters) {

        petDataSource.loadPetInfo(new IPetDataSource.ILoadPetInfoCallback(){
            @Override
            public void didLoad(List<InfoPet> pets) {
                final LoadAllPetsInfoUseCase.ResponseValues responseValues = new LoadAllPetsInfoUseCase.ResponseValues(pets);
                getCallback().onSuccess(responseValues);
            }

            @Override
            public void didFailLoad(DataLayerError error) {
                final DomainLayerError domainLayerError = getDomainLayerError(error);
                getCallback().onError(domainLayerError);
            }

        });
    }

    public static final class RequestValues implements UseCase.IRequestValues {
        public RequestValues() { }
    }

    public static final class ResponseValues implements UseCase.IResponseValues {
        private final List<InfoPet> pets;

        public ResponseValues(List<InfoPet> pets) {
            this.pets = pets;
        }

        public List<InfoPet> getPets() {
            return pets;
        }
    }

    private DomainLayerError getDomainLayerError(final DataLayerError dataLayerError) {
        String msg;
        switch (dataLayerError.getCode()){
            case "404":msg="Объект не был найден";break;
            case "500":msg="Серевер болен :(";break;
            default:msg="Упс, что-то произошло не так";break;
        }
        return new DomainLayerError(msg);
    }
}
