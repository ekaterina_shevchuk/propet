package com.example.propet.domain;

import com.example.propet.data.common.DataLayerError;
import com.example.propet.data.repo.pet.IPetDataSource;
import com.example.propet.domain.common.DomainLayerError;
import com.example.propet.domain.common.UseCase;
import com.example.propet.model.pet.Pet;

public class DeletePetUseCase extends UseCase<DeletePetUseCase.RequestValues, DeletePetUseCase.ResponseValues> {
    private final IPetDataSource petDataSource;

    public DeletePetUseCase(IPetDataSource petDataSource) {
        this.petDataSource = petDataSource;
    }

    @Override
    protected void execute(DeletePetUseCase.RequestValues parameters) {

        petDataSource.deletePet(new IPetDataSource.IDeletePetCallback(){
            @Override
            public void didDelete(String id) {
                final DeletePetUseCase.ResponseValues responseValues = new DeletePetUseCase.ResponseValues();
                getCallback().onSuccess(responseValues);
            }

            @Override
            public void didFailDelete(DataLayerError error) {
                final DomainLayerError domainLayerError = getDomainLayerError(error);
                getCallback().onError(domainLayerError);
            }

        }, parameters.getId());
    }


    public static final class RequestValues implements UseCase.IRequestValues {

        private final String id;

        public RequestValues(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }
    }

    public static final class ResponseValues implements UseCase.IResponseValues {

        public ResponseValues() {

        }

    }

    private DomainLayerError getDomainLayerError(final DataLayerError dataLayerError) {
        String msg;
        switch (dataLayerError.getCode()){
            case "404":msg="Объект не был найден";break;
            case "500":msg="Серевер болен :(";break;
            default:msg="Упс, что-то произошло не так";break;
        }
        return new DomainLayerError(msg);
    }
}

