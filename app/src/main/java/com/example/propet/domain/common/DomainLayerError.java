package com.example.propet.domain.common;

public class DomainLayerError {

    private String msg;
    public DomainLayerError( String msg) {
        this.msg = msg;
    }
    public String getMsg() {
        return msg;
    }
}
