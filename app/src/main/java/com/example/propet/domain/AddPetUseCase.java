package com.example.propet.domain;

import com.example.propet.data.common.DataLayerError;
import com.example.propet.data.repo.pet.IPetDataSource;
import com.example.propet.domain.common.DomainLayerError;
import com.example.propet.domain.common.UseCase;
import com.example.propet.model.pet.Pet;

public class AddPetUseCase extends UseCase<AddPetUseCase.RequestValues, AddPetUseCase.ResponseValues> {
    private final IPetDataSource petDataSource;

    public AddPetUseCase(IPetDataSource petDataSource) {
        this.petDataSource = petDataSource;
    }

    @Override
    protected void execute(AddPetUseCase.RequestValues parameters) {

        petDataSource.postPet(new IPetDataSource.IPostPetCallback() {
            @Override
            public void didLoad() {
                final AddPetUseCase.ResponseValues responseValues = new AddPetUseCase.ResponseValues();
                getCallback().onSuccess(responseValues);
            }

            @Override
            public void didFailLoad(DataLayerError error) {
                final DomainLayerError domainLayerError = getDomainLayerError(error);
                getCallback().onError(domainLayerError);
            }
        },parameters.getPet());
    }

    public static final class RequestValues implements UseCase.IRequestValues {

        private final Pet pet;

        public RequestValues(Pet pet) {
            this.pet = pet;
        }

        public Pet getPet() {
            return pet;
        }

    }

    public static final class ResponseValues implements UseCase.IResponseValues {
        public ResponseValues() { }
    }

    private DomainLayerError getDomainLayerError(final DataLayerError dataLayerError) {
        String msg;
        switch (dataLayerError.getCode()){
            case "000":msg="Данные о питомце не сохранены";break;
            case "404":msg="Объект не был найден";break;
            case "500":msg="Серевер болен :(";break;
            default:msg="Упс, что-то произошло не так";break;
        }
        return new DomainLayerError(msg);
    }
}
