package com.example.propet.domain;

import com.example.propet.data.common.DataLayerError;
import com.example.propet.data.repo.news.INewsDataSource;
import com.example.propet.domain.common.DomainLayerError;
import com.example.propet.domain.common.UseCase;
import com.example.propet.model.news.News;

import java.util.List;

public class LoadNewsUseCase extends UseCase<LoadNewsUseCase.RequestValues, LoadNewsUseCase.ResponseValues> {
    private final INewsDataSource newsRepository;

    public LoadNewsUseCase(final INewsDataSource notesRepository) {
        this.newsRepository = notesRepository;
    }

    @Override
    protected void execute(final RequestValues parameters) {
        newsRepository.loadNews(new INewsDataSource.ILoadNewsCallback() {

            @Override
            public void didLoad(final List<News> news) {
                final ResponseValues responseValues = new ResponseValues(news);
                // Этот callback мы передавали из Presenter в UseCaseExecutor.
                getCallback().onSuccess(responseValues);
            }

            // На уровне Repository произошла ошибка во время загрузки/получения news.
            @Override
            public void didFailLoad(final DataLayerError error) {
                final DomainLayerError domainLayerError = getDomainLayerError(error);
                getCallback().onError(domainLayerError);
            }
        });
    }

    //region Request and Response types.
    public static final class RequestValues implements UseCase.IRequestValues {
        // Входные данные на данном этапе не нужны.
        // Но для реализации UseCase'а нужна хотя бы пустая реализация.
    }

    public static final class ResponseValues implements UseCase.IResponseValues {
        private final List<News> news;

        public ResponseValues(final List<News> news) {
            this.news = news;
        }

        public List<News> getNews() {
            return news;
        }

    }
    //endregion

    private DomainLayerError getDomainLayerError(final DataLayerError dataLayerError) {
        // TODO: Method without implementation.
        return null;
    }
}
