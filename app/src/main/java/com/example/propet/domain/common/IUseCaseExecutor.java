package com.example.propet.domain.common;

/**
 * За данным интерфейсом мы будем скрывать наш синглтон UseCaseExecutor.
 */
public interface IUseCaseExecutor {

    <R extends UseCase.IRequestValues, S extends UseCase.IResponseValues> void execute(
            final UseCase<R, S> useCase, final R requestValues, final UseCase.IUseCaseCallback<S, DomainLayerError> callback);
}
