package com.example.propet.domain;

import com.example.propet.data.common.DataLayerError;
import com.example.propet.data.repo.user.IUserDataSource;
import com.example.propet.domain.common.DomainLayerError;
import com.example.propet.domain.common.UseCase;
import com.example.propet.model.user.User;

public class RegisterUserUseCase extends UseCase<RegisterUserUseCase.RequestValues,RegisterUserUseCase.ResponseValues>{

    private final IUserDataSource userDataSource;

    public RegisterUserUseCase(IUserDataSource userDataSource) {
        this.userDataSource = userDataSource;
    }

    @Override
    protected void execute(RequestValues parameters) {
        userDataSource.registerUser(new IUserDataSource.IRegisterCallback() {
            @Override
            public void didRegister() {
                final RegisterUserUseCase.ResponseValues responseValues = new RegisterUserUseCase.ResponseValues();
                getCallback().onSuccess(responseValues);
            }

            @Override
            public void didFailRegister(DataLayerError error) {
                final DomainLayerError domainLayerError = getDomainLayerError(error);
                getCallback().onError(domainLayerError);
            }
        },parameters.getUser());
    }


    public static final class RequestValues implements UseCase.IRequestValues {

        private final User user;

        public RequestValues(User user) {
            this.user = user;
        }

        public User getUser() {
            return user;
        }

    }

    public static final class ResponseValues implements UseCase.IResponseValues {
        public ResponseValues() { }
    }

    private DomainLayerError getDomainLayerError(final DataLayerError dataLayerError) {
        String msg;
        switch (dataLayerError.getCode()){
            case "401":msg="Ошибка авторизации";break;
            case "404":msg="Объект не был найден";break;
            case "500":msg="Серевер болен :(";break;
            default:msg="Упс, что-то произошло не так";break;
        }
        return new DomainLayerError(msg);
    }
}
