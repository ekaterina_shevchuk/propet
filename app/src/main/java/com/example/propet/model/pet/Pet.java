package com.example.propet.model.pet;

public class Pet {
    private String id;
    private String name;
    private Integer age;
    private String gender;
    private Integer weight;
    private String qualities;
    private String breed;
    private byte[] imageBytes;

    public Pet(String id, String name, Integer age, String gender, Integer weight, String qualities, String breed, byte[] imageBytes) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.weight = weight;
        this.qualities = qualities;
        this.breed = breed;
        this.imageBytes = imageBytes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {this.id = id;}

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public String getGender() { return gender; }

    public Integer getWeight() {
        return weight;
    }

    public String getQualities() {
        return qualities;
    }

    public String getBreed() {
        return breed;
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }
}
