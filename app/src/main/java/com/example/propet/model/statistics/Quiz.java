package com.example.propet.model.statistics;

import com.example.propet.model.common.Value;

public class Quiz {
    private final String quizId;
    private final String petId;
    private final Value value;
    private final String date; //format "dd.mm.yyyy"
    public Quiz(String quizId, String petId, Value value, String date) {
        this.quizId = quizId;
        this.petId = petId;
        this.value = value;
        this.date = date;
    }

    public Value getValue() {
        return value;
    }

    public String getDate() {
        return date;
    }

    public String getPetId() {
        return petId;
    }

    public String getQuizId() { return quizId; }
}
