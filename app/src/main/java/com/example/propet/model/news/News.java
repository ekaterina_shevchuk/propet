package com.example.propet.model.news;

import android.os.Parcel;
import android.os.Parcelable;

public final class News implements Parcelable {
    private final String newsId;
    private final NewsPriority newsPriority;
    private final String title;
    private final String createdDate;
    private final String content;
    private final byte[] imageBytes;

    public News(final String newsId,
                final NewsPriority newsPriority,
                final String title,
                final String createdDate,
                final String content,
                final byte[] imageBytes) {

        this.newsId = newsId;
        this.newsPriority = newsPriority;
        this.title = title;
        this.createdDate = createdDate;
        this.content = content;
        this.imageBytes = imageBytes;
    }

    protected News(Parcel in) {
        final String[] data = new String[3];
        in.readStringArray(data);

        this.newsId = data[0];
        this.newsPriority = NewsPriority.valueOf(data[1]);
        this.title = data[2];

        this.createdDate = (String) in.readSerializable();
        this.content = in.readString();

        final int bytesCount = in.readInt();
        final byte[] bytes = new byte[bytesCount];
        in.readByteArray(bytes);
        this.imageBytes = bytes;
    }

    public static final Creator<News> CREATOR = new Creator<News>() {
        @Override
        public News createFromParcel(final Parcel in) {
            return new News(in);
        }

        @Override
        public News[] newArray(int size) {
            return new News[size];
        }
    };

    public String getNewsId() {
        return newsId;
    }

    public NewsPriority getNewsPriority() {
        return newsPriority;
    }

    public String getTitle() {
        return title;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public String getContent() {
        return content;
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringArray(new String[] {
                this.newsId.toString(),
                this.newsPriority.name(),
                this.title
        });

        parcel.writeSerializable(this.createdDate);
        parcel.writeString(this.content);
        parcel.writeInt(this.imageBytes.length);
        parcel.writeByteArray(this.imageBytes);
    }
}
