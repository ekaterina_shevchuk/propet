package com.example.propet.model.news;

public enum NewsPriority {
    NOTE_PRIORITY_NOT_SET,
    NOTE_PRIORITY_LOW,
    NOTE_PRIORITY_DEFAULT,
    NOTE_PRIORITY_HIGH
}
