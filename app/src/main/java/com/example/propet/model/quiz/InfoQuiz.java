package com.example.propet.model.quiz;

public class InfoQuiz {
    private final String quizId;
    private final String title;
    private final String description;

    public InfoQuiz(String quizId, String title, String description) {
        this.quizId = quizId;
        this.title = title;
        this.description = description;
    }

    public String getQuizId() {
        return quizId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
