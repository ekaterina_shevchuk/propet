package com.example.propet.model.pet;

public class InfoPet {
    private final String id;
    private final String name;
    private final byte[] imageBytes;

    public InfoPet(String id, String name, byte[] imageBytes) {
        this.id = id;
        this.name = name;
        this.imageBytes = imageBytes;

    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public byte[] getImageBytes() { return imageBytes; }
}
