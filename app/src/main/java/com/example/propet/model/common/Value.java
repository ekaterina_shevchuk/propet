package com.example.propet.model.common;

public enum Value {
    LOW, MEDIUM, HIGH, ULTRA,
    SOFT, HARD, SMOOTH, STINKS,
    ANGRY, SLEEPY, SOFT_MOOD, SMILE
}
