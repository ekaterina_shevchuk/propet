package com.example.propet.model.statistics;

import com.example.propet.model.common.Value;

import java.util.Map;

public class Statistics{
    private final String title;
    private final Map<String,Value> map; //<date,val>

    public Statistics(String title, Map<String, Value> map) {
        this.title = title;
        this.map = map;
    }

    public String getTitle() {
        return title;
    }

    public Map<String, Value> getMap() {
        return map;
    }
}
