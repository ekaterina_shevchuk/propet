package com.example.propet.presentation.authorization;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.propet.R;
import com.example.propet.data.repo.user.IUserDataSource;
import com.example.propet.data.repo.user.UserRepository;
import com.example.propet.domain.AuthorizationUserUseCase;
import com.example.propet.domain.common.IUseCaseExecutor;
import com.example.propet.domain.common.UseCaseExecutor;
import com.example.propet.model.user.User;
import com.example.propet.presentation.common.PresentationLayerError;

public class AuthorizationActivity extends AppCompatActivity implements AuthModuleContract.View{

    private static final String LOG_TAG = AuthorizationActivity.class.getSimpleName();
    private AuthModuleContract.Presenter presenter;
    private AuthModuleContract.Router router;

    private EditText email;
    private EditText password;

    private final View.OnClickListener onClickListener_sign_up = v -> {
        User user = new User(email.getText().toString(),password.getText().toString());
        presenter.authorize(user);
    };

    private final View.OnClickListener onClickListener_sign_in = v -> router.goToMainScreen();

    private final View.OnClickListener onClickListener_recover = v -> router.goToRecoveryPasswordScreen();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        Log.d(LOG_TAG, "onCreate: ");

        final IUseCaseExecutor useCaseExecutor = UseCaseExecutor.getInstance();
        final IUserDataSource usersDataSource = UserRepository.getInstance(this);
        final AuthorizationUserUseCase authorizationUserUseCase = new AuthorizationUserUseCase(usersDataSource);
        final AuthPresenter presenter = new AuthPresenter(this, useCaseExecutor, authorizationUserUseCase);
        this.presenter = presenter;
        this.router = presenter;

        this.router.setActivity(this);

        email = findViewById(R.id.editText2);
        password = findViewById(R.id.editText3);

        Button sign_in = findViewById(R.id.button);
        sign_in.setOnClickListener(onClickListener_sign_in);

        Button sign_up = findViewById(R.id.button1);
        sign_up.setOnClickListener(onClickListener_sign_up);

        Button recover = findViewById(R.id.f_btn);
        recover.setOnClickListener(onClickListener_recover);
    }

    @Override
    public void show_sign_in_success(String msg) {
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void show_sign_in_fail(PresentationLayerError error) {
        Toast toast = Toast.makeText(this, error.getMsg(), Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void linkPresenter(AuthModuleContract.Presenter presenter) {
        this.presenter = presenter;
    }

    public static Intent getIntent(final Context context) {
        return new Intent(context, AuthorizationActivity.class);
    }


    @Override
    public void onDestroy() {
        presenter.onDestroy();
        Log.d(LOG_TAG, "onDestroy: ");
        presenter = null;
        super.onDestroy();
    }
}
