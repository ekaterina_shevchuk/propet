package com.example.propet.presentation.statistics;

import android.os.Bundle;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

import com.example.propet.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class StatisticsListActivity extends AppCompatActivity {
    private ImageButton imageButton3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics_list);

        imageButton3 = findViewById(R.id.imageButton3);

        ArrayList<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(1, 0));
        entries.add(new BarEntry(2, 1));
        entries.add(new BarEntry(1, 2));
        entries.add(new BarEntry(0, 3));

        BarDataSet dataset = new BarDataSet(entries, "Настроение");

        ArrayList<String> labels = new ArrayList<String>();
        labels.add("Игривый");
        labels.add("Ласковый");
        labels.add("Сонливый");
        labels.add("Злой");

        BarChart chart = new BarChart(StatisticsListActivity.this);
        setContentView(chart);

        //dataset.setColors(ColorTemplate.PASTEL_COLORS);

        BarData data = new BarData(labels, dataset);
        chart.setData(data);
        chart.animateY(5000);

    }
}