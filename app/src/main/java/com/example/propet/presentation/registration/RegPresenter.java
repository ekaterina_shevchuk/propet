package com.example.propet.presentation.registration;


import android.app.Activity;
import android.content.Intent;
import android.util.Log;


import com.example.propet.data.repo.user.UserRepository;
import com.example.propet.domain.RegisterUserUseCase;
import com.example.propet.domain.common.DomainLayerError;
import com.example.propet.domain.common.IUseCaseExecutor;
import com.example.propet.domain.common.UseCase;
import com.example.propet.model.user.User;
import com.example.propet.presentation.authorization.AuthorizationActivity;
import com.example.propet.presentation.common.PresentationLayerError;

public class RegPresenter implements RegModuleContract.Presenter,RegModuleContract.Router{

    private static final String LOG_TAG = RegPresenter.class.getCanonicalName();
    private final RegModuleContract.View view;
    private final IUseCaseExecutor useCaseExecutor;

    private Activity activity;
    private RegisterUserUseCase registerUserUseCase;

    public RegPresenter(RegModuleContract.View view, IUseCaseExecutor useCaseExecutor, RegisterUserUseCase registerUserUseCase) {
        this.view = view;
        this.useCaseExecutor = useCaseExecutor;
        this.registerUserUseCase = registerUserUseCase;
        view.linkPresenter(this);
    }


    @Override
    public void registration(User user) {
        Log.i(LOG_TAG, "Registration...");
        final RegisterUserUseCase.RequestValues requestValues = new RegisterUserUseCase.RequestValues(user);
        useCaseExecutor.execute(registerUserUseCase, requestValues, new UseCase.IUseCaseCallback<RegisterUserUseCase.ResponseValues, DomainLayerError>() {
            @Override
            public void onSuccess(RegisterUserUseCase.ResponseValues successResponse) {
                Log.i(LOG_TAG, "Registration: yes.");
                String msg = "Успешно!";
                view.show_sign_up_success(msg);
            }

            @Override
            public void onError(DomainLayerError error) {
                Log.i(LOG_TAG, "Registration:no.");
                view.show_sign_up_fail(getPresentationLayerError(error));
            }
        });
    }

    @Override
    public void onDestroy() {
        Log.v(LOG_TAG, "Destroying: nullify all use cases.");
        UserRepository.destroyInstance();
        registerUserUseCase = null;
    }

    @Override
    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void goToAuthorizationScreen() {
        final Intent intent = AuthorizationActivity.getIntent(activity);
        activity.startActivity(intent);
    }

    private PresentationLayerError getPresentationLayerError(final DomainLayerError domainLayerError) {
        return   new PresentationLayerError(domainLayerError.getMsg());
    }
}
