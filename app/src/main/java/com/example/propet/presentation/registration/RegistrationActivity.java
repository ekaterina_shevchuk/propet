package com.example.propet.presentation.registration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.propet.R;
import com.example.propet.data.repo.user.IUserDataSource;
import com.example.propet.data.repo.user.UserRepository;
import com.example.propet.domain.RegisterUserUseCase;
import com.example.propet.domain.common.IUseCaseExecutor;
import com.example.propet.domain.common.UseCaseExecutor;
import com.example.propet.model.user.User;
import com.example.propet.presentation.common.PresentationLayerError;

public class RegistrationActivity extends AppCompatActivity implements RegModuleContract.View{

    private static final String LOG_TAG = RegistrationActivity.class.getSimpleName();
    private RegModuleContract.Presenter presenter;
    private RegModuleContract.Router router;

    private EditText email;
    private EditText password;
    private EditText password_duplicate;


    private final View.OnClickListener onClickListener_sign_up = v -> {
        String p1 = password.getText().toString();
        String p2 = password_duplicate.getText().toString();
        if(p1.equals(p2)){
            User user = new User(email.getText().toString(),password.getText().toString());
            presenter.registration(user);
        }
        else{
            PresentationLayerError error = new PresentationLayerError("Пароли не совпадают");
            show_sign_up_fail(error);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regist);

        Log.d(LOG_TAG, "onCreate: ");

        final IUseCaseExecutor useCaseExecutor = UseCaseExecutor.getInstance();
        final IUserDataSource usersDataSource = UserRepository.getInstance(this);
        final RegisterUserUseCase registerUserUseCase = new RegisterUserUseCase(usersDataSource);
        final RegPresenter presenter = new RegPresenter(this, useCaseExecutor, registerUserUseCase);
        this.presenter = presenter;
        this.router = presenter;

        this.router.setActivity(this);

        email = findViewById(R.id.editText2);
        password = findViewById(R.id.editText3);
        password_duplicate = findViewById(R.id.editText4);

        Button sign_up = findViewById(R.id.button2);
        sign_up.setOnClickListener(onClickListener_sign_up);

    }


    @Override
    public void show_sign_up_success(String msg) {
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        toast.show();
        router.goToAuthorizationScreen();
    }

    @Override
    public void show_sign_up_fail(PresentationLayerError error) {
        Toast toast = Toast.makeText(this, error.getMsg(), Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void linkPresenter(RegModuleContract.Presenter presenter) {
        this.presenter = presenter;
    }

    public static Intent getIntent(final Context context) {
        return new Intent(context, RegistrationActivity.class);
    }


    @Override
    public void onDestroy() {
        presenter.onDestroy();
        Log.d(LOG_TAG, "onDestroy: ");
        presenter = null;
        super.onDestroy();
    }
}
