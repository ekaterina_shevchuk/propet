package com.example.propet.presentation.statistics;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

import com.example.propet.R;
import com.example.propet.presentation.pet.watch_all_pets_info.PetsPageActivity;
import com.example.propet.presentation.pet.watch_pet_info.PetInfoPageActivity;

public class StatisticInfoPageActivity extends AppCompatActivity {
    private ImageButton backButton, newsButton, quizButton, animalsButton, settingsButton;;
    private String petId;
    private Activity activity;

    private final View.OnClickListener onClickBackButton= v -> {
        final Intent intent = PetInfoPageActivity.getIntent(activity);
        intent.putExtra("id", petId);
        activity.startActivity(intent);
    };

    private final View.OnClickListener onClickAnimalButton= v -> {
        final Intent intent = PetsPageActivity.getIntent(activity);
        activity.startActivity(intent);
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pet_statistic_page);
        activity = this;

        newsButton = (ImageButton)findViewById(R.id.newsButton);
        quizButton = (ImageButton)findViewById(R.id.quizButton);
        animalsButton = (ImageButton)findViewById(R.id.animalsButton);
        settingsButton = (ImageButton)findViewById(R.id.settingsButton);

        animalsButton.setOnClickListener(onClickAnimalButton);
        petId = getIntent().getStringExtra("id");

        backButton = findViewById(R.id.imageButton3);
        backButton.setOnClickListener(onClickBackButton);
    }

    public static Intent getIntent(final Context context) {
        return new Intent(context, StatisticInfoPageActivity.class);
    }
}
