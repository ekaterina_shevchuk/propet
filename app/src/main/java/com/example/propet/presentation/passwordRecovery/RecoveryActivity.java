package com.example.propet.presentation.passwordRecovery;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.propet.R;
import com.example.propet.data.repo.user.IUserDataSource;
import com.example.propet.data.repo.user.UserRepository;
import com.example.propet.domain.RecoveryPasswordUseCase;
import com.example.propet.domain.common.IUseCaseExecutor;
import com.example.propet.domain.common.UseCaseExecutor;
import com.example.propet.presentation.common.PresentationLayerError;

public class RecoveryActivity extends AppCompatActivity implements RecModuleContract.View{

    private static final String LOG_TAG = RecoveryActivity.class.getSimpleName();
    private RecModuleContract.Presenter presenter;
    private RecModuleContract.Router router;

    private EditText email;

    private final View.OnClickListener onClickListener_recover = v -> presenter.recovery(email.getText().toString());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_recovery);

        Log.d(LOG_TAG, "onCreate: ");

        final IUseCaseExecutor useCaseExecutor = UseCaseExecutor.getInstance();
        final IUserDataSource usersDataSource = UserRepository.getInstance(this);
        final RecoveryPasswordUseCase recoveryPasswordUseCase = new RecoveryPasswordUseCase(usersDataSource);
        final RecPresenter presenter = new RecPresenter(this, useCaseExecutor, recoveryPasswordUseCase);
        this.presenter = presenter;
        this.router = presenter;

        this.router.setActivity(this);

        email = findViewById(R.id.editText2);

        Button recover = findViewById(R.id.button2);
        recover.setOnClickListener(onClickListener_recover);
    }

    @Override
    public void show_rec_success(String msg) {
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        toast.show();
        router.goToAuthorizationScreen();
    }

    @Override
    public void show_rec_fail(PresentationLayerError error) {
        Toast toast = Toast.makeText(this, error.getMsg(), Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void linkPresenter(RecModuleContract.Presenter presenter) {
        this.presenter = presenter;
    }

    public static Intent getIntent(final Context context) {
        return new Intent(context, RecoveryActivity.class);
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        Log.d(LOG_TAG, "onDestroy: ");
        presenter = null;
        super.onDestroy();
    }
}
