package com.example.propet.presentation.pet.watch_pet_info;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.propet.R;
import com.example.propet.data.repo.pet.IPetDataSource;
import com.example.propet.data.repo.pet.PetRepository;
import com.example.propet.domain.LoadPetInfoUseCase;
import com.example.propet.domain.common.IUseCaseExecutor;
import com.example.propet.domain.common.UseCaseExecutor;
import com.example.propet.model.pet.Pet;
import com.example.propet.presentation.common.PresentationLayerError;
import com.example.propet.presentation.newslist.NewsListActivity;
import com.example.propet.presentation.pet.watch_all_pets_info.PetsPageActivity;
import com.example.propet.presentation.quiz.QuizsActivity;
import com.example.propet.presentation.registration.RegistrationActivity;

public class PetInfoPageActivity extends AppCompatActivity implements PetInfoPageModuleContract.View{
    private static final String LOG_TAG = RegistrationActivity.class.getSimpleName();
    private PetInfoPageModuleContract.Presenter presenter;
    private PetInfoPageModuleContract.Router router;
    private TextView name, age, gender, weight, breed, features, featuresLabel, header;
    private Button showStatisticButton;
    private ImageView petPhoto;
    private ImageButton backButton, editButton, newsButton, quizButton, animalsButton, settingsButton;
    private String petId;

    private final View.OnClickListener onClickBackButton = v -> {
        router.goToPetsPageScreen();
    };

    private final View.OnClickListener onClickShowStatisticButton = v -> {
        router.goToStatisticInfoScreen(petId);
    };

    private final View.OnClickListener onClickEditDataButton = v -> {
        //todo
        router.goToChangePetInfoScreen();
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pet_info_page);
        Log.d(LOG_TAG, "onCreate: ");

        final IUseCaseExecutor useCaseExecutor = UseCaseExecutor.getInstance();
        final IPetDataSource petDataSource = PetRepository.getInstance(this);
        final LoadPetInfoUseCase loadPetDataUseCase = new LoadPetInfoUseCase(petDataSource);
        final PetInfoPagePresenter presenter = new PetInfoPagePresenter(this, useCaseExecutor, loadPetDataUseCase);
        this.presenter = presenter;
        this.router = presenter;

        name = (TextView)findViewById(R.id.textView8);
        age = (TextView)findViewById(R.id.textView9);
        gender = (TextView)findViewById(R.id.textView12);
        weight = (TextView)findViewById(R.id.textView13);
        breed = (TextView)findViewById(R.id.textView11);
        features = (TextView)findViewById(R.id.textView10);
        featuresLabel = (TextView)findViewById(R.id.textView6);
        header = (TextView)findViewById(R.id.textView);
        petPhoto = (ImageView)findViewById(R.id.imageView2);
        editButton = (ImageButton)findViewById(R.id.imageButton2);
        backButton = (ImageButton)findViewById(R.id.imageButton3);
        showStatisticButton = (Button)findViewById(R.id.button3);
        newsButton = (ImageButton)findViewById(R.id.newsButton);
        quizButton = (ImageButton)findViewById(R.id.quizButton);
        animalsButton = (ImageButton)findViewById(R.id.animalsButton);
        settingsButton = (ImageButton)findViewById(R.id.settingsButton);

        this.router.setActivity(this);
        petId = getIntent().getStringExtra("id");
        presenter.loadInfo(petId);
        backButton.setOnClickListener(onClickBackButton);
        editButton.setOnClickListener(onClickEditDataButton);
        showStatisticButton.setOnClickListener(onClickShowStatisticButton);
        animalsButton.setOnClickListener(onClickBackButton);

        newsButton = findViewById(R.id.newsButton);
        quizButton = findViewById(R.id.quizButton);

        quizButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(PetInfoPageActivity.this, QuizsActivity.class);
                startActivityForResult(intent, 100);
            }
        });

        newsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PetInfoPageActivity.this, NewsListActivity.class);
                startActivityForResult(intent, 101);
            }
        });

    }

    public static Intent getIntent(final Context context) {
        return new Intent(context, PetInfoPageActivity.class);
    }

    @Override
    public void show_load_info_success(String msg) {
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void show_load_info_fail(PresentationLayerError error) {
        Toast toast = Toast.makeText(this, error.getMsg(), Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void show_pet_info(Pet pet) {
        name.setText(pet.getName());
        age.setText(pet.getAge().toString());
        gender.setText(pet.getGender());
        weight.setText(pet.getWeight().toString());
        breed.setText(pet.getBreed());
        if(pet.getQualities().isEmpty()){
            featuresLabel.setVisibility(View.INVISIBLE);
            features.setVisibility(View.INVISIBLE);
        }
        else {
            featuresLabel.setVisibility(View.VISIBLE);
            features.setVisibility(View.VISIBLE);
            features.setText(pet.getQualities());
        }
        header.setText(pet.getName());
        setTestPhoto(pet.getId());
    }

    @Override
    public void linkPresenter(PetInfoPageModuleContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        Log.d(LOG_TAG, "onDestroy: ");
        presenter = null;
        super.onDestroy();
    }

    private void setTestPhoto(String id){
        if(id.equals("1"))
            petPhoto.setImageResource(R.drawable.snezhok);
        if(id.equals("2"))
            petPhoto.setImageResource(R.drawable.pushok);
        if(id.equals("3"))
            petPhoto.setImageResource(R.drawable.ugolek);
    }

    private void setPhoto(byte[] imageBytes){
        Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        petPhoto.setImageBitmap(Bitmap.createScaledBitmap(bitmap, petPhoto.getWidth(), petPhoto.getHeight(), false));
    }
}
