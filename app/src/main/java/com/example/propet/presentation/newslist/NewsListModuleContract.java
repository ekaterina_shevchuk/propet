package com.example.propet.presentation.newslist;

import android.app.Activity;

import com.example.propet.model.news.News;
import com.example.propet.presentation.common.IBasePresenter;
import com.example.propet.presentation.common.IBaseView;
import com.example.propet.presentation.common.PresentationLayerError;

import java.util.List;

public interface NewsListModuleContract {

    interface View extends IBaseView<Presenter> {
        void showNews(final List<News> items);
        void showNewsView();
        void showError(final PresentationLayerError error);
    }

    interface Presenter extends IBasePresenter {
        void loadNews();
        void onDestroy();
    }

    interface Router {
        void setActivity(final Activity activity);
        void goToTheDetailsScreen(final News item);
    }
}