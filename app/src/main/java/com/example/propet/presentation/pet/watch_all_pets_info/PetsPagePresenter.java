package com.example.propet.presentation.pet.watch_all_pets_info;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.example.propet.data.repo.pet.PetRepository;
import com.example.propet.domain.DeletePetUseCase;
import com.example.propet.domain.LoadAllPetsInfoUseCase;
import com.example.propet.domain.common.DomainLayerError;
import com.example.propet.domain.common.IUseCaseExecutor;
import com.example.propet.domain.common.UseCase;
import com.example.propet.presentation.common.PresentationLayerError;
import com.example.propet.presentation.pet.add_info.AddPetActivity;
import com.example.propet.presentation.pet.watch_pet_info.PetInfoPageActivity;

public class PetsPagePresenter implements PetsPageModuleContract.Presenter, PetsPageModuleContract.Router{
    private static final String LOG_TAG = PetsPagePresenter.class.getCanonicalName();
    private final PetsPageModuleContract.View view;
    private final IUseCaseExecutor useCaseExecutor;
    private final DeletePetUseCase deletePetUseCase;

    private Activity activity;
    private LoadAllPetsInfoUseCase loadPetDataUseCase;

    public PetsPagePresenter(PetsPageModuleContract.View view, IUseCaseExecutor useCaseExecutor, LoadAllPetsInfoUseCase loadPetDataUseCase, DeletePetUseCase deletePetUseCase) {
        this.view = view;
        this.useCaseExecutor = useCaseExecutor;
        this.loadPetDataUseCase = loadPetDataUseCase;
        this.deletePetUseCase = deletePetUseCase;
        view.linkPresenter(this);
    }

    @Override
    public void loadInfo() {
        Log.i(LOG_TAG, "Load some data about all pets...");
        LoadAllPetsInfoUseCase.RequestValues requestValuesLoadAllPetsInfoUseCase = new LoadAllPetsInfoUseCase.RequestValues();
        useCaseExecutor.execute(loadPetDataUseCase, requestValuesLoadAllPetsInfoUseCase, new UseCase.IUseCaseCallback<LoadAllPetsInfoUseCase.ResponseValues, DomainLayerError>() {

            @Override
            public void onSuccess(LoadAllPetsInfoUseCase.ResponseValues successResponse) {
                Log.i(LOG_TAG, "Data about pets have been loaded");
                String msg = "Успешно!";
                view.show_pets(successResponse.getPets());
                view.show_load_info_success(msg);
            }

            @Override
            public void onError(DomainLayerError error) {
                Log.i(LOG_TAG, "Data about pets haven't been loaded");
                view.show_load_info_fail(getPresentationLayerError(error));
            }
        });
    }

    @Override
    public void deleteInfo(String id) {
        Log.i(LOG_TAG, "Deleting data about pet...");
        DeletePetUseCase.RequestValues requestValuesDeletePetUseCase = new DeletePetUseCase.RequestValues(id);
        useCaseExecutor.execute(deletePetUseCase, requestValuesDeletePetUseCase, new UseCase.IUseCaseCallback<DeletePetUseCase.ResponseValues, DomainLayerError>() {

            @Override
            public void onSuccess(DeletePetUseCase.ResponseValues successResponse) {
                Log.i(LOG_TAG, "Data about pets have been deleted");
                String msg = "Успешно!";
                view.show_delete_info_success(msg);
            }

            @Override
            public void onError(DomainLayerError error) {
                Log.i(LOG_TAG, "Data about pets haven't been deleted");
                view.show_delete_info_fail(getPresentationLayerError(error));
            }
        });
    }

    @Override
    public void onDestroy() {
        Log.v(LOG_TAG, "Destroying: nullify all use cases.");
        PetRepository.destroyInstance();
        loadPetDataUseCase = null;
    }

    @Override
    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    private PresentationLayerError getPresentationLayerError(final DomainLayerError domainLayerError) {
        return new PresentationLayerError(domainLayerError.getMsg());
    }

    @Override
    public void goToPetInfoScreen(String id) {
        final Intent intent = PetInfoPageActivity.getIntent(activity);
        intent.putExtra("id", id);
        activity.startActivity(intent);
    }

    @Override
    public void goToAddPetInfoScreen() {
        final Intent intent = AddPetActivity.getIntent(activity);
        activity.startActivity(intent);
    }
}
