package com.example.propet.presentation.statistics;

import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.propet.model.statistics.Statistics;

public class StatisticsListItemViewHolder extends RecyclerView.ViewHolder {
    public StatisticsListItemViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    private static final String LOG_TAG = StatisticsListItemViewHolder.class.getCanonicalName();

    private Statistics item;

    void onBind(final Statistics item) {
        // TODO: Method without implementation
    }

    private View.OnClickListener onItemClickListener() {
        return view -> {
            if (null == item) {
                Log.e(LOG_TAG, "Statistic item is null in ViewHolder.");
                return;
            }
            // TODO: Method without implementation
        };
    }
}
