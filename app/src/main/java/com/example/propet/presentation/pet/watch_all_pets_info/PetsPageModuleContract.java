package com.example.propet.presentation.pet.watch_all_pets_info;

import android.app.Activity;

import com.example.propet.model.pet.InfoPet;
import com.example.propet.presentation.common.IBasePresenter;
import com.example.propet.presentation.common.IBaseView;
import com.example.propet.presentation.common.PresentationLayerError;

import java.util.List;

public class PetsPageModuleContract {
    interface View extends IBaseView<PetsPageModuleContract.Presenter> {
        void show_load_info_success(String msg);
        void show_load_info_fail(final PresentationLayerError error);
        void show_delete_info_success(String msg);
        void show_delete_info_fail(final PresentationLayerError error);
        void show_pets(List<InfoPet> petsInfo);
    }

    interface Presenter extends IBasePresenter {
        void deleteInfo(String id);
        void loadInfo();
        void onDestroy();
    }

    interface Router {
        void setActivity(final Activity activity);
        /**
         * Переход на экран для просмотра информации об определенном питомце
         */
        void goToPetInfoScreen(String id);
        /**
         * Переход на экран для добавления информации о питомце
         */
        void goToAddPetInfoScreen();
    }
}
