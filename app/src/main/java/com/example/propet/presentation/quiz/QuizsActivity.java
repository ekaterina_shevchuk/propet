package com.example.propet.presentation.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.example.propet.R;
import com.example.propet.presentation.pet.watch_all_pets_info.PetsPageActivity;
import com.example.propet.presentation.quiz1.Quiz11Activity;

import java.util.HashMap;

public class QuizsActivity extends AppCompatActivity {
    private HashMap<Button, String> viewMap;
    private Spinner spinner_animal;
    private ImageButton newsButton, quizButton, animalsButton;
    private Button goButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quizs);

        newsButton = findViewById(R.id.newsButton);
        quizButton = findViewById(R.id.quizButton);
        animalsButton = findViewById(R.id.animalsButton);
        goButton = findViewById(R.id.buttonGo);

        goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(QuizsActivity.this, Quiz11Activity.class);
                startActivityForResult(intent, 100);
            }
        });
        animalsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(QuizsActivity.this, PetsPageActivity.class);
                startActivityForResult(intent, 100);
            }
        });
        quizButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(QuizsActivity.this, QuizsActivity.class);
                startActivityForResult(intent, 100);
            }
        });

    }
}