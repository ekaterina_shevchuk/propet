package com.example.propet.presentation.statistics;

import com.example.propet.model.statistics.Statistics;
import com.example.propet.presentation.common.IBasePresenter;
import com.example.propet.presentation.common.IBaseView;
import com.example.propet.presentation.common.PresentationLayerError;

import java.util.List;

public interface StatisticsListModuleContract {
    interface View extends IBaseView<StatisticsListModuleContract.Presenter> {
        void showNotes(final List<Statistics> items);
        void showNoNotesView();
        void showError(final PresentationLayerError error);
    }

    interface Presenter extends IBasePresenter {
        void loadNotes();
        void onDestroy();
    }
}
