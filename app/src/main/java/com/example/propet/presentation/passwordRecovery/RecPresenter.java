package com.example.propet.presentation.passwordRecovery;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.example.propet.data.repo.user.UserRepository;
import com.example.propet.domain.RecoveryPasswordUseCase;
import com.example.propet.domain.common.DomainLayerError;
import com.example.propet.domain.common.IUseCaseExecutor;
import com.example.propet.domain.common.UseCase;
import com.example.propet.presentation.authorization.AuthorizationActivity;
import com.example.propet.presentation.common.PresentationLayerError;


public class RecPresenter implements RecModuleContract.Presenter,RecModuleContract.Router{

    private static final String LOG_TAG = RecPresenter.class.getCanonicalName();
    private final RecModuleContract.View view;
    private final IUseCaseExecutor useCaseExecutor;

    private Activity activity;
    private RecoveryPasswordUseCase recoveryPasswordUseCase;

    public RecPresenter(RecModuleContract.View view, IUseCaseExecutor useCaseExecutor, RecoveryPasswordUseCase useCase) {
        this.view = view;
        this.useCaseExecutor = useCaseExecutor;
        this.recoveryPasswordUseCase = useCase;
        view.linkPresenter(this);
    }

    @Override
    public void recovery(String email) {
        Log.i(LOG_TAG, "Recovery Password...");
        final RecoveryPasswordUseCase.RequestValues requestValues = new RecoveryPasswordUseCase.RequestValues(email);
        useCaseExecutor.execute(recoveryPasswordUseCase, requestValues, new UseCase.IUseCaseCallback<RecoveryPasswordUseCase.ResponseValues, DomainLayerError>() {
            @Override
            public void onSuccess(RecoveryPasswordUseCase.ResponseValues successResponse) {
                Log.i(LOG_TAG, "Recovery Password: yes.");
                String msg = "Проверьте почтовый ящик";
                view.show_rec_success(msg);
            }

            @Override
            public void onError(DomainLayerError error) {
                Log.i(LOG_TAG, "Registration:no.");
                view.show_rec_fail(getPresentationLayerError(error));
            }
        });
    }

    @Override
    public void onDestroy() {
        Log.v(LOG_TAG, "Destroying: nullify all use cases.");
        UserRepository.destroyInstance();
        recoveryPasswordUseCase = null;
    }

    @Override
    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void goToAuthorizationScreen() {
        final Intent intent = AuthorizationActivity.getIntent(activity);
        activity.startActivity(intent);
    }

    private PresentationLayerError getPresentationLayerError(final DomainLayerError domainLayerError) {
        return   new PresentationLayerError(domainLayerError.getMsg());
    }
}
