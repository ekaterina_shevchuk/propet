package com.example.propet.presentation.quiz1;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.example.propet.R;
import com.example.propet.data.repo.quiz.IQuizDataSource;
import com.example.propet.data.repo.quiz.QuizRepository;
import com.example.propet.domain.Quiz11UseCase;
import com.example.propet.domain.common.IUseCaseExecutor;
import com.example.propet.domain.common.UseCaseExecutor;
import com.example.propet.model.quiz.InfoQuiz;
import com.example.propet.presentation.newslist.NewsListActivity;
import com.example.propet.presentation.pet.add_info.AddPetActivity;
import com.example.propet.presentation.registration.RegistrationActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Quiz11Activity extends AppCompatActivity implements Quiz11ModuleContract.View {
    private static final String LOG_TAG = RegistrationActivity.class.getSimpleName();
    private Quiz11ModuleContract.Presenter presenter;
    private String item;
    private Quiz11ModuleContract.Router router;
    private final View.OnClickListener onClickSoft = v -> {
        router.goToNextQuiz();
    };
    private final View.OnClickListener onClickPets = v -> {
        router.goToPets();
    };
    private final View.OnClickListener onClickQuizs = v -> {
        router.goToQuizs();
    };
    private Spinner spinner_animal;
    private ImageView imageSoft, imageStinks, imageSmooth, imageHard;
    private ImageButton toBackPageButton, newsButton, quizButton, animalsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz11);
        Log.d(LOG_TAG, "onCreate: ");

        final IUseCaseExecutor useCaseExecutor = UseCaseExecutor.getInstance();
        final IQuizDataSource quizDataSource = QuizRepository.getInstance(this);
        final Quiz11UseCase quiz11UseCase = new Quiz11UseCase(quizDataSource);
        final Quiz11PagePresenter presenter = new Quiz11PagePresenter(this, useCaseExecutor, quiz11UseCase);
        this.presenter = presenter;
        this.router = presenter;

        this.router.setActivity(this);
        presenter.loadInfo("1");

        List<String> pets = new ArrayList<String>();
        pets.add("Снежок");
        pets.add("Пушок");
        pets.add("Уголек");
        this.spinner_animal = findViewById(R.id.spinner_animal);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, pets);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.spinner_animal.setAdapter(adapter);
        AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item = (String)parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        };
        spinner_animal.setOnItemSelectedListener(itemSelectedListener);

        this.imageSoft = findViewById(R.id.imageSoft);
        this.imageSmooth = findViewById(R.id.imageSmooth);
        this.imageStinks = findViewById(R.id.imageStinks);
        this.imageHard = findViewById(R.id.imageHard);

        imageSoft.setOnClickListener(onClickSoft);
        imageHard.setOnClickListener(onClickSoft);
        imageStinks.setOnClickListener(onClickSoft);
        imageSmooth.setOnClickListener(onClickSoft);

        toBackPageButton = findViewById(R.id.imageButton2);
        newsButton = findViewById(R.id.newsButton);
        quizButton = findViewById(R.id.quizButton);
        animalsButton = findViewById(R.id.animalsButton);

        newsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Quiz11Activity.this, NewsListActivity.class);
                startActivityForResult(intent, 101);
            }
        });
        animalsButton.setOnClickListener(onClickPets);
        quizButton.setOnClickListener(onClickQuizs);
        toBackPageButton.setOnClickListener(onClickQuizs);
    }

    @Override
    public void show_quiz_info(InfoQuiz infoQuiz) {

    }

    @Override
    public void linkPresenter(Quiz11ModuleContract.Presenter presenter) {
        this.presenter = presenter;
    }
}