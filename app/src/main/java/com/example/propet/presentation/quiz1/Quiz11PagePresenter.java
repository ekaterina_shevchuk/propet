package com.example.propet.presentation.quiz1;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.example.propet.data.repo.quiz.QuizRepository;
import com.example.propet.domain.Quiz11UseCase;
import com.example.propet.domain.common.DomainLayerError;
import com.example.propet.domain.common.IUseCaseExecutor;
import com.example.propet.domain.common.UseCase;
import com.example.propet.presentation.common.PresentationLayerError;
import com.example.propet.presentation.pet.watch_all_pets_info.PetsPageActivity;
import com.example.propet.presentation.quiz.QuizsActivity;

public class Quiz11PagePresenter implements Quiz11ModuleContract.Presenter, Quiz11ModuleContract.Router {
    private static final String LOG_TAG = Quiz11PagePresenter.class.getCanonicalName();
    private final Quiz11ModuleContract.View view;
    private final IUseCaseExecutor useCaseExecutor;
    private Quiz11UseCase.RequestValues requestValues;


    private Activity activity;
    private Quiz11UseCase quiz11UseCase;

    public Quiz11PagePresenter(Quiz11ModuleContract.View view, IUseCaseExecutor useCaseExecutor, Quiz11UseCase quiz11UseCase) {
        this.view = view;
        this.useCaseExecutor = useCaseExecutor;
        this.quiz11UseCase = quiz11UseCase;
        view.linkPresenter(this);
    }

    @Override
    public void loadInfo(String id) {
        Log.i(LOG_TAG, "Load data about quiz " + id);
        requestValues = new Quiz11UseCase.RequestValues(id);
        useCaseExecutor.execute(quiz11UseCase, requestValues, new UseCase.IUseCaseCallback<Quiz11UseCase.ResponseValues, DomainLayerError>() {

            @Override
            public void onSuccess(Quiz11UseCase.ResponseValues successResponse) {
                Log.i(LOG_TAG, "Data about pets have been loaded");
                //String msg = "Успешно!";
                //view.show_quiz_info(successResponse.getInfoQuiz());
            }

            @Override
            public void onError(DomainLayerError error) {
                Log.i(LOG_TAG, "Data about quiz 1 haven't been loaded");
            }
        });
    }

    @Override
    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void goToNextQuiz() {
        final Intent intent = new Intent(activity, Quiz12Activity.class);
        activity.startActivityForResult(intent, 100);
    }

    @Override
    public void goToQuizs() {
        final Intent intent = new Intent(activity, QuizsActivity.class);
        activity.startActivityForResult(intent, 100);
    }

    @Override
    public void goToPets() {
        final Intent intent = new Intent(activity, PetsPageActivity.class);
        activity.startActivityForResult(intent, 100);
    }

    private PresentationLayerError getPresentationLayerError(final DomainLayerError domainLayerError) {
        return new PresentationLayerError(domainLayerError.getMsg());
    }

    @Override
    public void onDestroy() {
        Log.v(LOG_TAG, "Destroying: nullify all use cases.");
        QuizRepository.destroyInstance();
        quiz11UseCase = null;
    }
}
