package com.example.propet.presentation.pet.add_info;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.propet.R;
import com.example.propet.data.repo.pet.IPetDataSource;
import com.example.propet.data.repo.pet.PetRepository;
import com.example.propet.domain.AddPetUseCase;
import com.example.propet.domain.common.IUseCaseExecutor;
import com.example.propet.domain.common.UseCaseExecutor;
import com.example.propet.model.pet.Pet;
import com.example.propet.presentation.common.PresentationLayerError;
import com.example.propet.presentation.newslist.NewsListActivity;
import com.example.propet.presentation.pet.watch_all_pets_info.PetsPageActivity;
import com.example.propet.presentation.quiz.QuizsActivity;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class AddPetActivity extends AppCompatActivity implements AddPetInfoModuleContract.View{
    private static final String LOG_TAG = AddPetActivity.class.getSimpleName();
    private AddPetInfoModuleContract.Presenter presenter;
    private AddPetInfoModuleContract.Router router;

    private EditText editTextName, editTextAge, editTextWeight, editTextFeatures, editTextBreed;
    private Spinner editGender;
    private Button addPetButton,loadImageButton;
    private ImageView petPhoto;
    private byte[] imageBytes;
    private ImageButton toBackPageButton, newsButton, quizButton, animalsButton, settingsButton;
    private final int Pick_image = 1;

    private final View.OnClickListener onClickAddPetButton = v -> {
        Pet pet = getPetInfo();
        if(pet != null)
            presenter.postPet(pet);
    };

    private final View.OnClickListener onClickBackPageButton = v -> {
        router.goToPetsPageScreen();
    };

    private final View.OnClickListener onClickLoadImageButton = v -> {
       loadImage();
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pet);
        Log.d(LOG_TAG, "onCreate: ");

        final IUseCaseExecutor useCaseExecutor = UseCaseExecutor.getInstance();
        final IPetDataSource petDataSource = PetRepository.getInstance(this);
        final AddPetUseCase addPetUseCase = new AddPetUseCase(petDataSource);
        final AddPetInfoPresenter presenter = new AddPetInfoPresenter(this, useCaseExecutor, addPetUseCase);
        this.presenter = presenter;
        this.router = presenter;

        this.router.setActivity(this);

        editTextName = (EditText)findViewById(R.id.editTextName);
        editTextAge =  (EditText)findViewById(R.id.editTextAge);
        editTextWeight = (EditText)findViewById(R.id.editTextWeight);
        editTextBreed = (EditText) findViewById(R.id.editBreed);
        editGender = (Spinner)findViewById(R.id.editGender);
        addPetButton = (Button)findViewById(R.id.addPetButton);
        loadImageButton =  (Button)findViewById(R.id.loadImageButton);
        editTextFeatures = (EditText)findViewById(R.id.editTextFeatures);
        petPhoto = (ImageView)findViewById(R.id.imageViewPet);
        toBackPageButton = (ImageButton)findViewById(R.id.imageButton2);
        newsButton = (ImageButton)findViewById(R.id.newsButton);
        quizButton = (ImageButton)findViewById(R.id.quizButton);
        animalsButton = (ImageButton)findViewById(R.id.animalsButton);
        settingsButton = (ImageButton)findViewById(R.id.settingsButton);

        animalsButton.setOnClickListener(onClickBackPageButton);
        toBackPageButton.setOnClickListener(onClickBackPageButton);
        loadImageButton.setOnClickListener(onClickLoadImageButton);
        addPetButton.setOnClickListener(onClickAddPetButton);

        newsButton = findViewById(R.id.newsButton);
        quizButton = findViewById(R.id.quizButton);

        quizButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(AddPetActivity.this, QuizsActivity.class);
                startActivityForResult(intent, 100);
            }
        });

        newsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddPetActivity.this, NewsListActivity.class);
                startActivityForResult(intent, 101);
            }
        });


    }


    public Pet getPetInfo() {
        String name = editTextName.getText().toString();
        String breed = editTextBreed.getText().toString();
        String gender = editGender.getSelectedItem().toString();
        String features = editTextFeatures.getText().toString();
        Integer weight = null;
        Integer age = null;

        if(editTextWeight.length() != 0 )
            weight = Integer.parseInt(editTextWeight.getText().toString());
        if(editTextAge.length() != 0)
            age = Integer.parseInt(editTextAge.getText().toString());

        if(name.isEmpty() || weight == null || age == null || breed.isEmpty() || gender.isEmpty() ) {
            Toast.makeText(getApplicationContext(), "Заполните все поля!", Toast.LENGTH_LONG).show();
            return null;
        }
        else {
            Pet pet = new Pet("0", name, age, gender, weight, features, breed, new byte[]{});
            //  pet = new Pet(counterId.toString(), name, age, gender, weight, features, breed, imageBytes);
            return pet;
        }
    }

    private void loadImage(){
        //Вызываем стандартную галерею для выбора изображения с помощью Intent.ACTION_PICK:
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        //Тип получаемых объектов - image:
        photoPickerIntent.setType("image/*");
        //Запускаем переход с ожиданием обратного результата в виде информации об изображении:
        startActivityForResult(photoPickerIntent, Pick_image);
    }

    //Обрабатываем результат выбора в галерее:
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case Pick_image:
                if (resultCode == RESULT_OK) {
                    try {
                        //Получаем URI изображения, преобразуем его в Bitmap
                        //объект и отображаем в элементе ImageView нашего интерфейса:
                        final Uri imageUri = imageReturnedIntent.getData();
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        imageBytes =convertBitmapToByteArray(this, selectedImage);
                        petPhoto.setImageBitmap(selectedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
        }
    }

    public static Intent getIntent(final Context context) {
        return new Intent(context, AddPetActivity.class);
    }

    public byte[] convertBitmapToByteArray(Context context, Bitmap bitmap) {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream(bitmap.getWidth() * bitmap.getHeight());
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, buffer);
        return buffer.toByteArray();
    }

    @Override
    public void show_post_success(String msg) {
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void show_post_fail(PresentationLayerError error) {
        Toast toast = Toast.makeText(this, error.getMsg(), Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void linkPresenter(AddPetInfoModuleContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        Log.d(LOG_TAG, "onDestroy: ");
        presenter = null;
        super.onDestroy();
    }
}