package com.example.propet.presentation.registration;

import android.app.Activity;

import com.example.propet.model.user.User;
import com.example.propet.presentation.common.IBasePresenter;
import com.example.propet.presentation.common.IBaseView;
import com.example.propet.presentation.common.PresentationLayerError;

public interface RegModuleContract {
    interface View extends IBaseView<RegModuleContract.Presenter> {
        /**
         * Метод, который вызывается Presenter'ом, когда регистрация завершилась успешно
         */
        void show_sign_up_success(String msg);

        /**
         * Метод, который вызывается Presenter'ом, когда регистрация завершась ошибкой
         */
        void show_sign_up_fail(final PresentationLayerError error);
    }

    interface Presenter extends IBasePresenter {
        /**
         *Презентер отправляет на проверку данные для регистрации
         */
        void registration(User user);
        void onDestroy();
    }

    interface Router {
        void setActivity(final Activity activity);
        /**
         * Когда пользователь нажал кнопку "регистрация и все отработало удачно"
         */
        void goToAuthorizationScreen();
    }
}
