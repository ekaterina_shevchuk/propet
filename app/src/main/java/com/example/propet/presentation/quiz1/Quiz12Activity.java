package com.example.propet.presentation.quiz1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.propet.R;
import com.example.propet.presentation.newslist.NewsListActivity;
import com.example.propet.presentation.pet.add_info.AddPetActivity;
import com.example.propet.presentation.pet.watch_all_pets_info.PetsPageActivity;
import com.example.propet.presentation.quiz.QuizsActivity;

import java.util.ArrayList;
import java.util.List;

public class Quiz12Activity extends AppCompatActivity {
    private Spinner spinner_animal;
    private ImageView imageSmile, imageSleepy, imageAngry, imageSoftMood;
    private ImageButton toBackPageButton, newsButton, quizButton, animalsButton;
    private final View.OnClickListener quizeOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Toast toast = Toast.makeText(Quiz12Activity.this, "Записано", Toast.LENGTH_SHORT);
            toast.show();
            final Intent intent = new Intent(Quiz12Activity.this, QuizsActivity.class);
            startActivityForResult(intent, 100);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz12);

        List<String> pets = new ArrayList<String>();
        pets.add("Снежок");
        pets.add("Пушок");
        pets.add("Уголек");
        this.spinner_animal = findViewById(R.id.spinner_animal12);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, pets);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.spinner_animal.setAdapter(adapter);
        this.imageAngry = findViewById(R.id.imageAngry);
        this.imageSleepy = findViewById(R.id.imageSleepy);
        this.imageSoftMood = findViewById(R.id.imageSoftMood);
        this.imageSmile = findViewById(R.id.imageSmile);

        imageAngry.setOnClickListener(quizeOnClickListener);
        imageSleepy.setOnClickListener(quizeOnClickListener);
        imageSoftMood.setOnClickListener(quizeOnClickListener);
        imageSmile.setOnClickListener(quizeOnClickListener);

        toBackPageButton = findViewById(R.id.imageButton2);
        newsButton = findViewById(R.id.newsButton);
        quizButton = findViewById(R.id.quizButton);
        animalsButton = findViewById(R.id.animalsButton);

        newsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Quiz12Activity.this, NewsListActivity.class);
                startActivityForResult(intent, 101);
            }
        });
        animalsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(Quiz12Activity.this, PetsPageActivity.class);
                startActivityForResult(intent, 100);
            }
        });
        quizButton.setOnClickListener(quizeOnClickListener);
        toBackPageButton.setOnClickListener(quizeOnClickListener);
    }
}