package com.example.propet.presentation.newslist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.propet.R;
import com.example.propet.model.news.News;

import java.util.List;

public final class NewsListAdapter extends RecyclerView.Adapter<NewsListItemViewHolder> {

    private final NewsListModuleContract.Router router;
    private List<News> items;

    public NewsListAdapter(final NewsListModuleContract.Router router, final List<News> items) {
        this.router = router;
        this.items = items;
    }

    void replaceData(final List<News> items) {
        if (null != items) {
            this.items = items;
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NewsListItemViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        final Context context = parent.getContext();

        final View view = LayoutInflater
                .from(context)
                .inflate(R.layout.item_list_news, parent, false);

        return new NewsListItemViewHolder(view, router);
    }

    @Override
    public void onBindViewHolder(@NonNull final NewsListItemViewHolder holder, final int position) {
        final News item = items.get(position);
        holder.onBind(item);
    }

    @Override
    public int getItemCount() {
        return (null != items) ? items.size() : 0;
    }
}
