package com.example.propet.presentation.newslist;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.propet.R;
import com.example.propet.data.repo.news.INewsDataSource;
import com.example.propet.data.repo.news.NewsRepository;
import com.example.propet.domain.LoadNewsUseCase;
import com.example.propet.domain.common.IUseCaseExecutor;
import com.example.propet.domain.common.UseCaseExecutor;
import com.example.propet.model.news.News;
import com.example.propet.presentation.common.PresentationLayerError;
import com.example.propet.presentation.pet.watch_all_pets_info.PetsPageActivity;
import com.example.propet.presentation.quiz.QuizsActivity;

import java.util.ArrayList;
import java.util.List;

public final class NewsListActivity extends AppCompatActivity implements NewsListModuleContract.View {

    private static final String LOG_TAG = NewsListActivity.class.getSimpleName();
    private View quizButton;
    private View animalsButton;
    private NewsListModuleContract.Presenter presenter;

    private RecyclerView recyclerView;
    private NewsListAdapter listAdapter;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_list);

        Log.d(LOG_TAG, "onCreate: ");

        quizButton = findViewById(R.id.quizButton);
        animalsButton = findViewById(R.id.animalsButton);

        animalsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(NewsListActivity.this, PetsPageActivity.class);
                startActivityForResult(intent, 100);
            }
        });
        quizButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(NewsListActivity.this, QuizsActivity.class);
                startActivityForResult(intent, 100);
            }
        });


        final IUseCaseExecutor useCaseExecutor = UseCaseExecutor.getInstance();
        final INewsDataSource newsDataSource = NewsRepository.getInstance();
        final LoadNewsUseCase loadNewsUseCase = new LoadNewsUseCase(newsDataSource);

        final NewsListPresenter presenter = new NewsListPresenter(this, useCaseExecutor, loadNewsUseCase);
        this.presenter = presenter;

        presenter.setActivity(this);

        recyclerView = findViewById(R.id.recycler_view_notes);
        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        listAdapter = new NewsListAdapter(presenter, new ArrayList<>());
        recyclerView.setAdapter(listAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "onStart: ");
        // Code...
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "onResume: ");
        // Show progress HUD
        presenter.loadNews();
    }

    @Override
    protected void onPause() {
        // Code...
        Log.d(LOG_TAG, "onPause: ");
        super.onPause();
    }

    @Override
    protected void onStop() {
        // Code...
        Log.d(LOG_TAG, "onStop: ");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        Log.d(LOG_TAG, "onDestroy: ");
        presenter = null;
        super.onDestroy();
    }

    @Override
    public void linkPresenter(final NewsListModuleContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showNews(final List<News> items) {
        listAdapter.replaceData(items);
    }

    @Override
    public void showNewsView() {
        // Hide progress hud
        // TODO: Method without implementation
    }

    @Override
    public void showError(final PresentationLayerError error) {
        // Hide progress hud
        final Toast toast = new Toast(this);
        toast.setText(error.getMsg());
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }
}