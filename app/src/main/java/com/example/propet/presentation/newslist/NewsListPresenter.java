package com.example.propet.presentation.newslist;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.example.propet.data.repo.news.NewsRepository;
import com.example.propet.domain.LoadNewsUseCase;
import com.example.propet.domain.common.DomainLayerError;
import com.example.propet.domain.common.IUseCaseExecutor;
import com.example.propet.domain.common.UseCase;
import com.example.propet.model.news.News;
import com.example.propet.presentation.common.PresentationLayerError;

import java.util.List;

public final class NewsListPresenter implements NewsListModuleContract.Presenter, NewsListModuleContract.Router {

    private static final String LOG_TAG = NewsListPresenter.class.getCanonicalName();

    private final NewsListModuleContract.View view;
    private final IUseCaseExecutor useCaseExecutor;

    private Activity activity;
    private LoadNewsUseCase loadNewsUseCase;

    public NewsListPresenter(final NewsListModuleContract.View view,
                              final IUseCaseExecutor useCaseExecutor,
                              final LoadNewsUseCase loadNewsUseCase) {

        this.view = view;
        this.useCaseExecutor = useCaseExecutor;
        this.loadNewsUseCase = loadNewsUseCase;

        view.linkPresenter(this);
    }

    @Override
    public void onDestroy() {
        Log.v(LOG_TAG, "Destroying NotesRepository and nullify all use cases.");
        // Можем прокидывать команду в UseCase, можем делать и в Presenter'е.
        NewsRepository.destroyInstance();
        loadNewsUseCase = null;
    }

    @Override
    public void loadNews() {
        Log.i(LOG_TAG, "Loading notes with.");
        final LoadNewsUseCase.RequestValues requestValues = new LoadNewsUseCase.RequestValues();

        useCaseExecutor.execute(loadNewsUseCase, requestValues, new UseCase.IUseCaseCallback<LoadNewsUseCase.ResponseValues, DomainLayerError>() {
            @Override
            public void onSuccess(final LoadNewsUseCase.ResponseValues successResponse) {
                final List<News> notes = successResponse.getNews();
                Log.d(LOG_TAG, "Successfully obtained " + notes.size() + " notes.");
                view.showNews(notes);
            }

            @Override
            public void onError(final DomainLayerError error) {
                Log.e(LOG_TAG, "Error occurred while loading notes. Details: " + error.toString());
                final PresentationLayerError presentationLayerError = getPresentationLayerError(error);
                view.showError(presentationLayerError);
            }
        });
    }

    @Override
    public void setActivity(final Activity activity) {
        this.activity = activity;
    }

    @Override
    public void goToTheDetailsScreen(final News news) {
        //final Intent intent = NoteDetailsActivity.getIntent(activity, note);
        //activity.startActivity(intent);
    }

    private PresentationLayerError getPresentationLayerError(final DomainLayerError domainLayerError) {
        // TODO: Method without implementation
        return null;
    }
}