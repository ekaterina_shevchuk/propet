package com.example.propet.presentation.pet.add_info;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.example.propet.data.repo.pet.PetRepository;
import com.example.propet.domain.AddPetUseCase;
import com.example.propet.domain.common.DomainLayerError;
import com.example.propet.domain.common.IUseCaseExecutor;
import com.example.propet.domain.common.UseCase;
import com.example.propet.model.pet.Pet;
import com.example.propet.presentation.common.PresentationLayerError;
import com.example.propet.presentation.pet.watch_all_pets_info.PetsPageActivity;


public class AddPetInfoPresenter implements AddPetInfoModuleContract.Presenter, AddPetInfoModuleContract.Router{
    private static final String LOG_TAG = AddPetInfoPresenter.class.getCanonicalName();
    private final AddPetInfoModuleContract.View view;
    private final IUseCaseExecutor useCaseExecutor;

    private Activity activity;
    private AddPetUseCase addPetUseCase;

    @Override
    public void postPet(Pet pet) {
        Log.i(LOG_TAG, "Add info about pet...");
        final AddPetUseCase.RequestValues requestValues = new AddPetUseCase.RequestValues(pet);
        useCaseExecutor.execute(addPetUseCase, requestValues, new UseCase.IUseCaseCallback<AddPetUseCase.ResponseValues, DomainLayerError>() {
            @Override
            public void onSuccess(AddPetUseCase.ResponseValues successResponse) {
                Log.i(LOG_TAG, "Info about pet have saved");
                String msg = "Данные сохранены";
                view.show_post_success(msg);
            }

            @Override
            public void onError(DomainLayerError error) {
                Log.i(LOG_TAG, "Info about pet haven't saved");
                view.show_post_fail(getPresentationLayerError(error));
            }
        });
    }

    @Override
    public void onDestroy() {
        Log.v(LOG_TAG, "Destroying: nullify all use cases.");
        PetRepository.destroyInstance();
        addPetUseCase = null;
    }

    public AddPetInfoPresenter(final AddPetInfoModuleContract.View view,
                               final IUseCaseExecutor useCaseExecutor,
                               final AddPetUseCase addPetUseCase) {
        this.view = view;
        this.useCaseExecutor = useCaseExecutor;
        this.addPetUseCase = addPetUseCase;
        view.linkPresenter(this);
    }


    private PresentationLayerError getPresentationLayerError(final DomainLayerError domainLayerError) {
        return   new PresentationLayerError(domainLayerError.getMsg());
    }

    @Override
    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void goToPetsPageScreen() {
        final Intent intent = PetsPageActivity.getIntent(activity);
        activity.startActivity(intent);
    }

}
