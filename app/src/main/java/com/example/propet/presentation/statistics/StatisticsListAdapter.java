package com.example.propet.presentation.statistics;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.propet.model.statistics.Statistics;

import java.util.List;

public class StatisticsListAdapter extends RecyclerView.Adapter<StatisticsListItemViewHolder> {
    private List<Statistics> items;

    public StatisticsListAdapter(final List<Statistics> items) {
        this.items = items;
    }

    void replaceData(final List<Statistics> items) {
        if (null != items) {
            this.items = items;
        }
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public StatisticsListItemViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        // TODO: Method without implementation
        return null;
    }


    @Override
    public void onBindViewHolder(@NonNull final StatisticsListItemViewHolder holder, final int position) {
        final Statistics item = items.get(position);
        holder.onBind(item);
    }

    @Override
    public int getItemCount() {
        return (null != items) ? items.size() : 0;
    }
}

