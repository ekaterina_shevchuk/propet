package com.example.propet.presentation.authorization;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.example.propet.data.repo.user.UserRepository;
import com.example.propet.domain.AuthorizationUserUseCase;
import com.example.propet.domain.common.DomainLayerError;
import com.example.propet.domain.common.IUseCaseExecutor;
import com.example.propet.domain.common.UseCase;
import com.example.propet.model.user.User;
import com.example.propet.presentation.common.PresentationLayerError;
import com.example.propet.presentation.newslist.NewsListActivity;
import com.example.propet.presentation.passwordRecovery.RecoveryActivity;
import com.example.propet.presentation.pet.watch_all_pets_info.PetsPageActivity;
import com.example.propet.presentation.quiz.QuizsActivity;
import com.example.propet.presentation.registration.RegistrationActivity;


public class AuthPresenter implements AuthModuleContract.Presenter,AuthModuleContract.Router {

    private static final String LOG_TAG = AuthPresenter.class.getCanonicalName();
    private final AuthModuleContract.View view;
    private final IUseCaseExecutor useCaseExecutor;

    private Activity activity;
    private AuthorizationUserUseCase authorizationUserUseCase;

    @Override
    public void authorize(User user) {
        Log.i(LOG_TAG, "Authorization...");
        final AuthorizationUserUseCase.RequestValues requestValues = new AuthorizationUserUseCase.RequestValues(user);
        useCaseExecutor.execute(authorizationUserUseCase, requestValues, new UseCase.IUseCaseCallback<AuthorizationUserUseCase.ResponseValues, DomainLayerError>() {
            @Override
            public void onSuccess(AuthorizationUserUseCase.ResponseValues successResponse) {
                Log.i(LOG_TAG, "Authorization: yes.");
                String msg = "Добро пожаловать!";
                view.show_sign_in_success(msg);
            }

            @Override
            public void onError(DomainLayerError error) {
                Log.i(LOG_TAG, "Authorization:no.");
                view.show_sign_in_fail(getPresentationLayerError(error));
            }
        });

    }

    @Override
    public void onDestroy() {
        Log.v(LOG_TAG, "Destroying: nullify all use cases.");
        UserRepository.destroyInstance();
        authorizationUserUseCase = null;
    }

    public AuthPresenter(final AuthModuleContract.View view,
                             final IUseCaseExecutor useCaseExecutor,
                             final AuthorizationUserUseCase userUseCase) {
        this.view = view;
        this.useCaseExecutor = useCaseExecutor;
        this.authorizationUserUseCase = userUseCase;
        view.linkPresenter(this);
    }


    private PresentationLayerError getPresentationLayerError(final DomainLayerError domainLayerError) {
        return   new PresentationLayerError(domainLayerError.getMsg());
    }

    @Override
    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void goToRegistrationScreen() {
        final Intent intent = RegistrationActivity.getIntent(activity);
        activity.startActivity(intent);
    }

    @Override
    public void goToRecoveryPasswordScreen() {
        final Intent intent = RecoveryActivity.getIntent(activity);
        activity.startActivity(intent);
    }

    @Override
    public void goToMainScreen() {
        final Intent intent = new Intent(activity, NewsListActivity.class); //NewsListActivity.getIntent(activity);
        activity.startActivity(intent);
    }
}
