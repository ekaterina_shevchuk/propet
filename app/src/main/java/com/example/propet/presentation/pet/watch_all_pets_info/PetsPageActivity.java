package com.example.propet.presentation.pet.watch_all_pets_info;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.propet.R;
import com.example.propet.data.repo.pet.IPetDataSource;
import com.example.propet.data.repo.pet.PetRepository;
import com.example.propet.domain.DeletePetUseCase;
import com.example.propet.domain.LoadAllPetsInfoUseCase;
import com.example.propet.domain.common.IUseCaseExecutor;
import com.example.propet.domain.common.UseCaseExecutor;
import com.example.propet.model.pet.InfoPet;
import com.example.propet.presentation.common.PresentationLayerError;
import com.example.propet.presentation.newslist.NewsListActivity;
import com.example.propet.presentation.quiz.QuizsActivity;
import com.example.propet.presentation.quiz1.Quiz12Activity;
import com.example.propet.presentation.registration.RegistrationActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.HashMap;
import java.util.List;

public class PetsPageActivity extends AppCompatActivity implements PetsPageModuleContract.View{
    private static final String LOG_TAG = RegistrationActivity.class.getSimpleName();
    private PetsPageModuleContract.Presenter presenter;
    private PetsPageModuleContract.Router router;


    private ImageButton newsButton, quizButton, animalsButton, settingsButton;
    private FloatingActionButton addPetButton;
    private HashMap<Button, String> viewMap;
    private LinearLayout linearLayout;

    private final View.OnClickListener onClickAddButton = v -> {
        router.goToAddPetInfoScreen();
    };

    private final View.OnClickListener onClickPetFrame = v -> {
        String petId = viewMap.get(v);
        router.goToPetInfoScreen(petId);
    };

    private final View.OnLongClickListener onLongClickPetFrame = v -> {
        String petId = viewMap.get(v);
        presenter.deleteInfo(petId);
        onResume();
        return true;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pets_page);

        Log.d(LOG_TAG, "onCreate: ");

        linearLayout = (LinearLayout) findViewById(R.id.linear);
        final IUseCaseExecutor useCaseExecutor = UseCaseExecutor.getInstance();
        final IPetDataSource petDataSource = PetRepository.getInstance(this);
        final LoadAllPetsInfoUseCase loadAllPetsDataUseCase = new LoadAllPetsInfoUseCase(petDataSource);
        final DeletePetUseCase deletePetUseCase = new DeletePetUseCase(petDataSource);
        final PetsPagePresenter presenter = new PetsPagePresenter(this, useCaseExecutor, loadAllPetsDataUseCase, deletePetUseCase);
        this.presenter = presenter;
        this.router = presenter;

        this.router.setActivity(this);
        viewMap = new HashMap<>();
        presenter.loadInfo();

        addPetButton = (FloatingActionButton)findViewById(R.id.addPetButton);
        addPetButton.setOnClickListener(onClickAddButton);

        newsButton = findViewById(R.id.newsButton);
        quizButton = findViewById(R.id.quizButton);

        quizButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(PetsPageActivity.this, QuizsActivity.class);
                startActivityForResult(intent, 100);
            }
        });

        newsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PetsPageActivity.this, NewsListActivity.class);
                startActivity(intent);
                //startActivityForResult(intent, 100);
            }
        });
    }

    public static Intent getIntent(final Context context) {
        return new Intent(context, PetsPageActivity.class);
    }

    @Override
    public void show_load_info_success(String msg) {
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void show_load_info_fail(PresentationLayerError error) {
        Toast toast = Toast.makeText(this, error.getMsg(), Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void show_delete_info_success(String msg) {
        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void show_delete_info_fail(PresentationLayerError error) {
        Toast toast = Toast.makeText(this, error.getMsg(), Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void show_pets(List<InfoPet> petsInfo) {
        if(viewMap != null)
            viewMap.clear();

        for (InfoPet petInfo : petsInfo) {
            final View view = getLayoutInflater().inflate(R.layout.custom_edittext_layout, null);
            Button button = (Button) view.findViewById(R.id.button);
            ImageView image = view.findViewById(R.id.imageView);
            button.setText(petInfo.getName());
            button.setOnClickListener(onClickPetFrame);
            button.setOnLongClickListener(onLongClickPetFrame);
            viewMap.put(button, petInfo.getId());

            //set photo by bytes array start (don't work correct)
            if (petInfo.getImageBytes().length != 0){
                Bitmap bmp = BitmapFactory.decodeByteArray(petInfo.getImageBytes(), 0, petInfo.getImageBytes().length);
                image.setImageBitmap(Bitmap.createScaledBitmap(bmp, image.getWidth(), image.getHeight(), false));
             }
            //end

            //set test photo start
            if(petInfo.getId().equals("1"))
                image.setImageResource(R.drawable.snezhok);
            if(petInfo.getId().equals("2"))
                image.setImageResource(R.drawable.pushok);
            if(petInfo.getId().equals("3"))
                image.setImageResource(R.drawable.ugolek);
            //end

            linearLayout.addView(view);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        linearLayout.removeAllViews();
        presenter.loadInfo();
    }

    @Override
    public void linkPresenter(PetsPageModuleContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        Log.d(LOG_TAG, "onDestroy: ");
        presenter = null;
        super.onDestroy();
    }
}