package com.example.propet.presentation.newslist;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.propet.R;
import com.example.propet.model.news.News;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class NewsListItemViewHolder extends RecyclerView.ViewHolder {

    private static final String LOG_TAG = NewsListItemViewHolder.class.getCanonicalName();

    private TextView titleTextView;
    private TextView contentTextView;
    private TextView dateCreatedTextView;

    private final NewsListModuleContract.Router router;
    private News item;

    public NewsListItemViewHolder(@NonNull final View itemView, final NewsListModuleContract.Router router) {
        super(itemView);

        this.router = router;

        titleTextView = itemView.findViewById(R.id.title_text_view);
        contentTextView = itemView.findViewById(R.id.content_text_view);
        dateCreatedTextView = itemView.findViewById(R.id.date_created_text_view);

        itemView.setOnClickListener(onItemClickListener());
    }

    void onBind(final News item) {
        this.item = item;

        titleTextView.setText(item.getTitle());
        contentTextView.setText(item.getContent());

        final String dateString = item.getCreatedDate();
        dateCreatedTextView.setText(dateString);
    }

    private View.OnClickListener onItemClickListener() {
        return view -> {
            if (null == item) {
                Log.e(LOG_TAG, "Note item is null in ViewHolder.");
                return;
            }
            router.goToTheDetailsScreen(item);
        };
    }
}
