package com.example.propet.presentation.passwordRecovery;

import android.app.Activity;

import com.example.propet.model.user.User;
import com.example.propet.presentation.common.IBasePresenter;
import com.example.propet.presentation.common.IBaseView;
import com.example.propet.presentation.common.PresentationLayerError;

public interface RecModuleContract {
    interface View extends IBaseView<RecModuleContract.Presenter> {
        /**
         * Метод, который вызывается Presenter'ом, когда отправка письма завершилась успешно
         */
        void show_rec_success(String msg);

        /**
         * Метод, который вызывается Presenter'ом, когда отправка письма завершась ошибкой
         */
        void show_rec_fail(final PresentationLayerError error);
    }

    interface Presenter extends IBasePresenter {
        /**
         *Презентер отправляет на проверку данные для регистрации
         */
        void recovery(String email);
        void onDestroy();
    }

    interface Router {
        void setActivity(final Activity activity);
        /**
         * Когда пользователь нажал кнопку "отправить и все отработало удачно"
         */
        void goToAuthorizationScreen();
    }
}
