package com.example.propet.presentation.quiz1;

import android.app.Activity;

import com.example.propet.model.quiz.InfoQuiz;
import com.example.propet.presentation.common.IBasePresenter;
import com.example.propet.presentation.common.IBaseView;

public class Quiz11ModuleContract {
    interface View extends IBaseView<Quiz11ModuleContract.Presenter> {
        void show_quiz_info(InfoQuiz infoQuiz);
    }

    interface Presenter extends IBasePresenter {
        void loadInfo(String id);
        void onDestroy();
    }

    interface Router {
        void setActivity(final Activity activity);

        /**
         * Когда пользователь нажал на один из вариантов
         */
        void goToNextQuiz();
        void goToQuizs();
        void goToPets();
    }
}
