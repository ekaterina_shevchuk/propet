package com.example.propet.presentation.common;

public class PresentationLayerError {
    private final String msg;

    public PresentationLayerError(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
