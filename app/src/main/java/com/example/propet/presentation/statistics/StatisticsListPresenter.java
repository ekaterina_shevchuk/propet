package com.example.propet.presentation.statistics;

import android.util.Log;

import com.example.propet.data.repo.news.NewsRepository;
import com.example.propet.domain.LoadStatisticsUseCase;
import com.example.propet.domain.common.DomainLayerError;
import com.example.propet.domain.common.IUseCaseExecutor;
import com.example.propet.domain.common.UseCase;
import com.example.propet.model.statistics.Statistics;
import com.example.propet.presentation.common.PresentationLayerError;

import java.util.List;

public class StatisticsListPresenter implements StatisticsListModuleContract.Presenter{

    private static final String LOG_TAG = StatisticsListPresenter.class.getCanonicalName();

    private final StatisticsListModuleContract.View view;
    private final IUseCaseExecutor useCaseExecutor;

    private LoadStatisticsUseCase loadStatisticsUseCase;

    public StatisticsListPresenter(final StatisticsListModuleContract.View view,
                             final IUseCaseExecutor useCaseExecutor,
                             final LoadStatisticsUseCase loadStatisticsUseCase) {

        this.view = view;
        this.useCaseExecutor = useCaseExecutor;
        this.loadStatisticsUseCase = loadStatisticsUseCase;

        view.linkPresenter(this);
    }

    @Override
    public void loadNotes() {
        Log.i(LOG_TAG, "Loading statistics with.");
        final LoadStatisticsUseCase.RequestValues requestValues = new LoadStatisticsUseCase.RequestValues();

        useCaseExecutor.execute(loadStatisticsUseCase, requestValues, new UseCase.IUseCaseCallback<LoadStatisticsUseCase.ResponseValues, DomainLayerError>() {
            @Override
            public void onSuccess(final LoadStatisticsUseCase.ResponseValues successResponse) {
                final List<Statistics> statistics = successResponse.getStatistics();
                Log.d(LOG_TAG, "Successfully obtained " + statistics.size() + " statistics.");
                view.showNotes(statistics);
            }

            @Override
            public void onError(final DomainLayerError error) {
                Log.e(LOG_TAG, "Error occurred while loading statistics. Details: " + error.toString());
                final PresentationLayerError presentationLayerError = getPresentationLayerError(error);
                view.showError(presentationLayerError);
            }
        });
    }

    @Override
    public void onDestroy() {
        Log.v(LOG_TAG, "Destroying StatisticsRepository and nullify all use cases.");
        // Можем прокидывать в UseCase, можем делать и в Presenter'е.
        NewsRepository.destroyInstance();
        loadStatisticsUseCase = null;
    }

    private PresentationLayerError getPresentationLayerError(final DomainLayerError domainLayerError) {
        // TODO: Method without implementation
        return null;
    }
}
