package com.example.propet.presentation.pet.watch_pet_info;

import android.app.Activity;

import com.example.propet.model.pet.InfoPet;
import com.example.propet.model.pet.Pet;
import com.example.propet.presentation.common.IBasePresenter;
import com.example.propet.presentation.common.IBaseView;
import com.example.propet.presentation.common.PresentationLayerError;
import com.example.propet.presentation.pet.watch_all_pets_info.PetsPageModuleContract;

import java.util.List;

public class PetInfoPageModuleContract {
    interface View extends IBaseView<PetInfoPageModuleContract.Presenter> {
        void show_load_info_success(String msg);
        void show_load_info_fail(final PresentationLayerError error);
        void show_pet_info(Pet pet);
    }

    interface Presenter extends IBasePresenter {
        void loadInfo(String id);
        void onDestroy();
    }

    interface Router {
        void setActivity(final Activity activity);
        /**
         * Переход на экран для просмотра информации о добавленных питомцах
         */
        void goToPetsPageScreen();
        /**
         * Переход на экран для изменения информации о питомце
         */
        void goToChangePetInfoScreen();
        /**
         * Переход на экран для просмотра статистической информации по питомцу
         */
        void goToStatisticInfoScreen(String id);
    }
}
