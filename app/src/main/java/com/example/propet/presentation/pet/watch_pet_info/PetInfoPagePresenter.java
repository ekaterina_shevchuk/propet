package com.example.propet.presentation.pet.watch_pet_info;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.example.propet.data.repo.pet.PetRepository;
import com.example.propet.domain.LoadPetInfoUseCase;
import com.example.propet.domain.common.DomainLayerError;
import com.example.propet.domain.common.IUseCaseExecutor;
import com.example.propet.domain.common.UseCase;
import com.example.propet.presentation.common.PresentationLayerError;
import com.example.propet.presentation.pet.watch_all_pets_info.PetsPageActivity;
import com.example.propet.presentation.statistics.StatisticInfoPageActivity;
import com.example.propet.presentation.statistics.StatisticsListActivity;

public class PetInfoPagePresenter implements PetInfoPageModuleContract.Presenter, PetInfoPageModuleContract.Router{
    private static final String LOG_TAG = PetInfoPagePresenter.class.getCanonicalName();
    private final PetInfoPageModuleContract.View view;
    private final IUseCaseExecutor useCaseExecutor;
    private LoadPetInfoUseCase.RequestValues requestValues;


    private Activity activity;
    private LoadPetInfoUseCase loadPetInfoUseCase;

    public PetInfoPagePresenter(PetInfoPageModuleContract.View view, IUseCaseExecutor useCaseExecutor, LoadPetInfoUseCase loadPetInfoUseCase) {
        this.view = view;
        this.useCaseExecutor = useCaseExecutor;
        this.loadPetInfoUseCase = loadPetInfoUseCase;
        view.linkPresenter(this);
    }

    @Override
    public void loadInfo(String id) {
        Log.i(LOG_TAG, "Load data about pet...");
        requestValues = new LoadPetInfoUseCase.RequestValues(id);
        useCaseExecutor.execute(loadPetInfoUseCase, requestValues, new UseCase.IUseCaseCallback<LoadPetInfoUseCase.ResponseValues, DomainLayerError>() {

            @Override
            public void onSuccess(LoadPetInfoUseCase.ResponseValues successResponse) {
                Log.i(LOG_TAG, "Data about pets have been loaded");
                String msg = "Успешно!";
                view.show_pet_info(successResponse.getPet());
                view.show_load_info_success(msg);
            }

            @Override
            public void onError(DomainLayerError error) {
                Log.i(LOG_TAG, "Data about pets haven't been loaded");
                view.show_load_info_fail(getPresentationLayerError(error));
            }
        });
    }

    @Override
    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void goToPetsPageScreen() {
        final Intent intent = PetsPageActivity.getIntent(activity);
        activity.startActivity(intent);
    }

    @Override
    public void goToChangePetInfoScreen() {
        //todo
    }

    @Override
    public void goToStatisticInfoScreen(String id) {
        final Intent intent = new Intent(activity, StatisticsListActivity.class);
        //final Intent intent = StatisticInfoPageActivity.getIntent(activity);
        intent.putExtra("id", id);
        activity.startActivity(intent);
    }

    private PresentationLayerError getPresentationLayerError(final DomainLayerError domainLayerError) {
        return new PresentationLayerError(domainLayerError.getMsg());
    }

    @Override
    public void onDestroy() {
        Log.v(LOG_TAG, "Destroying: nullify all use cases.");
        PetRepository.destroyInstance();
        loadPetInfoUseCase = null;
    }
}
