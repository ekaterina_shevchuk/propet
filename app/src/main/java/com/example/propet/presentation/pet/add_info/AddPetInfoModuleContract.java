package com.example.propet.presentation.pet.add_info;

import android.app.Activity;

import com.example.propet.model.pet.Pet;
import com.example.propet.presentation.common.IBasePresenter;
import com.example.propet.presentation.common.IBaseView;
import com.example.propet.presentation.common.PresentationLayerError;

public class AddPetInfoModuleContract {
    interface View extends IBaseView<AddPetInfoModuleContract.Presenter> {
        void show_post_success(String msg);
        void show_post_fail(final PresentationLayerError error);
    }

    interface Presenter extends IBasePresenter {
        void postPet(Pet pet);
        void onDestroy();
    }

    interface Router {
        void setActivity(final Activity activity);
        /**
         * Переход на экран для просмотра информации о добавленных питомцах
         */
        void goToPetsPageScreen();
    }

}
