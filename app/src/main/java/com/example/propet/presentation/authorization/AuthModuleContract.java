package com.example.propet.presentation.authorization;

import android.app.Activity;
import android.content.Context;

import com.example.propet.model.news.News;
import com.example.propet.model.user.User;
import com.example.propet.presentation.common.IBasePresenter;
import com.example.propet.presentation.common.IBaseView;
import com.example.propet.presentation.common.PresentationLayerError;


import java.util.List;

public interface AuthModuleContract {
    interface View extends IBaseView<AuthModuleContract.Presenter> {
        /**
         * Метод, который вызывается Presenter'ом, когда авторизация завершилась успешно
         */
        void show_sign_in_success(String msg);

        /**
         * Метод, который вызывается Presenter'ом, когда авторизация завершась ошибкой
         */
        void show_sign_in_fail(final PresentationLayerError error);
    }

    interface Presenter extends IBasePresenter {
        /**
         *Презентер отправляет на проверку данные для входа,
         *по идее должны получать обратно токен, но у нас жеж нет нормального бэка :)
         * поэтому просто ок - не ок.
         */
        void authorize(User user);
        void onDestroy();
    }

    interface Router {
        void setActivity(final Activity activity);

        /**
         * Когда пользователь нажал кнопку "регистрация"
         */
        void goToRegistrationScreen();

        /**
         * Когда пользователь нажал кнопку "забыл пароль"
         */
        void goToRecoveryPasswordScreen();
        /**
         * Когда пользователь нажал кнопку "войти и все отработало удачно"
         */
        void goToMainScreen();
    }
}
